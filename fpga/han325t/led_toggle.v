// @copyright Copyright (c) 2022 OnMicro Corp.
// @brief     Toggle LEDs to indicate that FPGA device is programmed.
// @author    wei.lu@onmicro.com.cn
// @license   SPDX-License-Identifier: Apache-2.0

//-----------------------------------------------------------------
// Module:  LED toggle
//-----------------------------------------------------------------
module led_toggle
//-----------------------------------------------------------------
// Params
//-----------------------------------------------------------------
#(
    parameter PERIOD_CNT = 32'd50000000
)
//-----------------------------------------------------------------
// Ports
//-----------------------------------------------------------------
(
    // Inputs
     input          clk_i
    ,input          rst_i

    // Outputs
    ,output [1:0]   led_o
);

reg led_q;
reg [31:0]led_count;

//counter control
always @ (posedge clk_i or negedge rst_i)
if (!rst_i)
    led_count <= 32'd0;
else if (led_count == PERIOD_CNT)
    led_count <= 32'd0;
else
    led_count <= led_count+32'd1;

//led output register control
always @ (posedge clk_i or negedge rst_i)
if (!rst_i)
    led_q <= 1'b0;
else if (led_count < PERIOD_CNT/2)
    led_q <= 1'b1;
else
    led_q <= 1'b0;

assign led_o[0] = led_q;
assign led_o[1] = ~led_o[0];

endmodule // led_toggle
