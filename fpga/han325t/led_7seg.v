// @copyright Copyright (c) 2022 OnMicro Corp.
// @brief     7-Segment LED with 74AHC595 driver.
// @author    wei.lu@onmicro.com.cn
// @license   SPDX-License-Identifier: Apache-2.0

//
// 74hc595 shift register driver - 18-11-2021
//

module led_74hc595_driver (
    input i_clk,        // system clock 48 Mhz
    input [7:0] i_Data, // data to shift out
    input i_Enable,     // enable shifter
    output o_Ready,     // shifter ready
    output o_RCLK,      // 74hc595 latch clock
    output o_SRCLK,     // 74hc595 serial clock
    output o_SER );     // 74hc595 serial out

reg r_RCLK            = 0;
reg r_SRCLK           = 0;
reg r_Ready           = 1;

reg [8:0] r_shifter   = 0; // shifter register
reg [3:0] r_shiftcnt  = 0; // shift counter 0..7
reg [3:0] s_state     = 0; // state 0..7

assign o_Ready        = r_Ready;
assign o_RCLK         = r_RCLK;
assign o_SRCLK        = r_SRCLK;
assign o_SER          = r_shifter[8];

//
// Shift state machine
//
always @ (posedge i_clk) begin
    case (s_state)
        0: begin // idle, wait for i_enable
            if (i_Enable == 1) begin
                r_shifter[7:0] <= i_Data;
                s_state        <= 1;
                r_shiftcnt     <= 0;
                r_Ready        <= 0;
            end
        end
        1: begin // shift left 1 position
            r_shifter <= r_shifter << 1;
            s_state   <= 2;
        end   
        2: begin // wait one clock cycle 
            s_state <= 3;
        end
        3: begin // SRCLK = 1
            r_SRCLK <= 1;
            s_state <= 4;
        end   
        4: begin // SRCLK = 0
            r_SRCLK <= 0;
            s_state <= 5;
        end
        5: begin // check of all shift out
            if (r_shiftcnt == 7) begin
                s_state <= 6;
            end else begin
                r_shiftcnt <= r_shiftcnt + 1;
                s_state <= 1;
            end
        end
        6: begin // RCLK = 1
            r_RCLK <= 1;
            s_state <= 7;
        end
        7: begin // RCLK = 0
            r_RCLK <= 0;
            s_state <= 8;
        end
        8: begin // Ready = 1
            r_Ready <= 1;
            s_state <= 0;
        end 
    endcase  
end

endmodule // led_74hc595_driver


//
// 74HC595 by ShiYuGuLu on 2021/08/25
//

module HC595_Driver (
    input clk,
    input rst,
    input [15:0] data,
    input S_EN,
    output o_shcp,
    output o_stcp,
    output o_ds
);

reg shcp;
reg stcp;
reg ds;

reg [7:0]    diver_cnt;
wire         sck_plus;
reg [5:0]    shcp_edge_cnt;
reg [15:0]   rdata;

parameter    CNT_MAX = 2;

always @ (posedge clk)
    if (S_EN)
        rdata <= data;

always @ (posedge clk or negedge rst)
    if (!rst)
        diver_cnt <= 0;
    else if (diver_cnt == CNT_MAX - 1'b1)
        diver_cnt <= 0;
    else
        diver_cnt <= diver_cnt + 1'b1;

    assign sck_plus = (diver_cnt == CNT_MAX - 1'b1);

always @ (posedge clk or negedge rst)
    if (!rst)
        shcp_edge_cnt <= 0;
    else if (sck_plus)
    begin
        if (shcp_edge_cnt == 6'd32)
            shcp_edge_cnt <= 0;
        else
            shcp_edge_cnt <= shcp_edge_cnt + 1'b1;
    end
    else
        shcp_edge_cnt <= shcp_edge_cnt;

always @ (posedge clk or negedge rst)
    if (!rst) begin
        stcp <= 0;
        ds   <= 0;
        shcp <= 0;
    end
    else begin
        case (shcp_edge_cnt)
            0:  begin shcp       <= 0; stcp <= 0; ds <= rdata[15]; end
            1:  begin shcp       <= 1; end
            2:  begin shcp       <= 0; ds <= rdata[14]; end
            3:  begin shcp       <= 1; end
            4:  begin shcp       <= 0; ds <= rdata[13]; end
            5:  begin shcp       <= 1; end
            6:  begin shcp       <= 0; ds <= rdata[12]; end
            7:  begin shcp       <= 1; end
            8:  begin shcp       <= 0; ds <= rdata[11]; end
            9:  begin shcp       <= 1; end
            10: begin shcp       <= 0; ds <= rdata[10]; end
            11: begin shcp       <= 1; end
            12: begin shcp       <= 0; ds <= rdata[9]; end
            13: begin shcp       <= 1; end
            14: begin shcp       <= 0; ds <= rdata[8]; end
            15: begin shcp       <= 1; end
            16: begin shcp       <= 0; ds <= rdata[7]; end
            17: begin shcp       <= 1; end
            18: begin shcp       <= 0; ds <= rdata[6]; end
            19: begin shcp       <= 1; end
            20: begin shcp       <= 0; ds <= rdata[5]; end
            21: begin shcp       <= 1; end
            22: begin shcp       <= 0; ds <= rdata[4]; end
            23: begin shcp       <= 1; end
            24: begin shcp       <= 0; ds <= rdata[3]; end
            25: begin shcp       <= 1; end
            26: begin shcp       <= 0; ds <= rdata[2]; end
            27: begin shcp       <= 1; end
            28: begin shcp       <= 0; ds <= rdata[1]; end
            29: begin shcp       <= 1; end
            30: begin shcp       <= 0; ds <= rdata[0]; end
            31: begin shcp       <= 1; end
            32: begin stcp       <= 1'b1; end
            default: begin shcp <= 0; stcp <= 0; ds <= 0; end
        endcase // case (shcp_edge_cnt)
    end // else: !if(!rst)

assign o_shcp = shcp;
assign o_stcp = stcp;
assign o_ds = ds;

endmodule // HC595_Driver


/***************************************************************
         **************name: seg_595************
              *********author: zzuxzt*********
         **************time: 2014.5.21***************
* enhanced by wei.lu@onmicro.com.cn on 2022/10/09
***************************************************************/
module seg_595(
     input i_clk
    ,input i_rst_n
    ,input [15:0] i_data
    ,output o_shcp   //shift clk
    ,output o_stcp   //latch clk
    ,output o_ds
); 

/********************Common code****************/
//------------duan xuan------------------------
// +-a-+
// f   b
// +-g-+
// e   c
// +-d-+ dp
//共阳极数码管：低电平选中各数码段 [7:0]=[dp,g,f,e,d,c,b,a]
parameter SEG_BLANK = 8'hff,
          SEG_NUM0 = 8'hc0,
          SEG_NUM1 = 8'hf9,
          SEG_NUM2 = 8'ha4,
          SEG_NUM3 = 8'hb0,
          SEG_NUM4 = 8'h99,
          SEG_NUM5 = 8'h92,
          SEG_NUM6 = 8'h82,
          SEG_NUM7 = 8'hF8,
          SEG_NUM8 = 8'h80,
          SEG_NUM9 = 8'h90,
          SEG_NUMA = 8'h88,
          SEG_NUMB = 8'h83,
          SEG_NUMC = 8'hc6,
          SEG_NUMD = 8'ha1,
          SEG_NUME = 8'h86,
          SEG_NUMF = 8'h8e;
//共阴极数码管：高电平选中各数码段 [7:0]=[dp,g,f,e,d,c,b,a]
//parameter SEG_NUM0 = 8'h3f,
//          SEG_NUM1 = 8'h06,
//          SEG_NUM2 = 8'h5b,
//          SEG_NUM3 = 8'h4f,
//          SEG_NUM4 = 8'h66,
//          SEG_NUM5 = 8'h6d,
//          SEG_NUM6 = 8'h7d,
//          SEG_NUM7 = 8'h07,
//          SEG_NUM8 = 8'h7f,
//          SEG_NUM9 = 8'h6f,
//          SEG_NUMA = 8'h77,
//          SEG_NUMB = 8'h7c,
//          SEG_NUMC = 8'h39,
//          SEG_NUMD = 8'h5e,
//          SEG_NUME = 8'h79,
//          SEG_NUMF = 8'h71;

/****************************seg display data**********************************/
//---------------------data conventer----------------------------
reg [7:0] digit[3:0];
always @ (posedge i_clk or negedge i_rst_n)
begin
    if (!i_rst_n) begin
        digit[0] <= SEG_BLANK;
        digit[1] <= SEG_BLANK;
        digit[2] <= SEG_BLANK;
        digit[3] <= SEG_BLANK;
    end
    else begin
        case (i_data[3:0])
            4'h0: digit[0] <= SEG_NUM0;
            4'h1: digit[0] <= SEG_NUM1;
            4'h2: digit[0] <= SEG_NUM2;
            4'h3: digit[0] <= SEG_NUM3;
            4'h4: digit[0] <= SEG_NUM4;
            4'h5: digit[0] <= SEG_NUM5;
            4'h6: digit[0] <= SEG_NUM6;
            4'h7: digit[0] <= SEG_NUM7;
            4'h8: digit[0] <= SEG_NUM8;
            4'h9: digit[0] <= SEG_NUM9;
            4'ha: digit[0] <= SEG_NUMA;
            4'hb: digit[0] <= SEG_NUMB;
            4'hc: digit[0] <= SEG_NUMC;
            4'hd: digit[0] <= SEG_NUMD;
            4'he: digit[0] <= SEG_NUME;
            4'hf: digit[0] <= SEG_NUMF;
            default: digit[0] <= SEG_BLANK;
        endcase
        case (i_data[7:4])
            4'h0: digit[1] <= SEG_NUM0;
            4'h1: digit[1] <= SEG_NUM1;
            4'h2: digit[1] <= SEG_NUM2;
            4'h3: digit[1] <= SEG_NUM3;
            4'h4: digit[1] <= SEG_NUM4;
            4'h5: digit[1] <= SEG_NUM5;
            4'h6: digit[1] <= SEG_NUM6;
            4'h7: digit[1] <= SEG_NUM7;
            4'h8: digit[1] <= SEG_NUM8;
            4'h9: digit[1] <= SEG_NUM9;
            4'ha: digit[1] <= SEG_NUMA;
            4'hb: digit[1] <= SEG_NUMB;
            4'hc: digit[1] <= SEG_NUMC;
            4'hd: digit[1] <= SEG_NUMD;
            4'he: digit[1] <= SEG_NUME;
            4'hf: digit[1] <= SEG_NUMF;
            default: digit[1] <= SEG_BLANK;
        endcase
        case (i_data[11:8])
            4'h0: digit[2] <= SEG_NUM0;
            4'h1: digit[2] <= SEG_NUM1;
            4'h2: digit[2] <= SEG_NUM2;
            4'h3: digit[2] <= SEG_NUM3;
            4'h4: digit[2] <= SEG_NUM4;
            4'h5: digit[2] <= SEG_NUM5;
            4'h6: digit[2] <= SEG_NUM6;
            4'h7: digit[2] <= SEG_NUM7;
            4'h8: digit[2] <= SEG_NUM8;
            4'h9: digit[2] <= SEG_NUM9;
            4'ha: digit[2] <= SEG_NUMA;
            4'hb: digit[2] <= SEG_NUMB;
            4'hc: digit[2] <= SEG_NUMC;
            4'hd: digit[2] <= SEG_NUMD;
            4'he: digit[2] <= SEG_NUME;
            4'hf: digit[2] <= SEG_NUMF;
            default: digit[2] <= SEG_BLANK;
        endcase
        case (alpha[3:0]) //change every second
            4'h0: digit[3] <= SEG_NUM0;
            4'h1: digit[3] <= SEG_NUM1;
            4'h2: digit[3] <= SEG_NUM2;
            4'h3: digit[3] <= SEG_NUM3;
            4'h4: digit[3] <= SEG_NUM4;
            4'h5: digit[3] <= SEG_NUM5;
            4'h6: digit[3] <= SEG_NUM6;
            4'h7: digit[3] <= SEG_NUM7;
            4'h8: digit[3] <= SEG_NUM8;
            4'h9: digit[3] <= SEG_NUM9;
            4'ha: digit[3] <= SEG_NUMA;
            4'hb: digit[3] <= SEG_NUMB;
            4'hc: digit[3] <= SEG_NUMC;
            4'hd: digit[3] <= SEG_NUMD;
            4'he: digit[3] <= SEG_NUME;
            4'hf: digit[3] <= SEG_NUMF;
            default: digit[3] <= SEG_BLANK;
        endcase
    end
end

//------------------1s counter-----------------------
//alpha每秒增1，从A递增至F，再回卷至A.
reg [31:0] cnt;
reg [7:0] alpha;
always @ (posedge i_clk or negedge i_rst_n)
begin
    if (!i_rst_n) begin
        cnt <= 1'b0;
        alpha <= 8'ha;
    end
    else if (cnt == 32'd50000000) begin
        cnt <= 1'b0;
        if (alpha == 8'hf)
            alpha <= 8'ha;
        else
            alpha <= alpha + 1'b1;
    end
    else
	cnt <= cnt + 1'b1;
end	

/*****************************seg driver*************************************/
//---------------shift clk----------------------
//串行位移时钟: shcp = i_clk / 5
reg [2:0] cnt_st;
reg shcp_r;
always @ (posedge i_clk or negedge i_rst_n)
begin
    if (!i_rst_n) begin
        cnt_st <= 1'b0;
        shcp_r <= 1'b1;
    end
    else if (cnt_st == 3'd4) begin
        cnt_st <= 3'd0;
        shcp_r <= ~shcp_r;      //10M
    end
         else
             cnt_st <= cnt_st + 1'b1;
end

//--------------------capture the shift clk trailing edge----------------------
reg shcp_r0,shcp_r1;
wire shcp_flag;
always @ (posedge i_clk or negedge i_rst_n)
begin
    if (!i_rst_n) begin
        shcp_r0 <= 1'b1;
        shcp_r1 <= 1'b1;
    end
    else begin
        shcp_r0 <= shcp_r;
        shcp_r1 <= shcp_r0;
    end
end 

assign shcp_flag = (~shcp_r0 && shcp_r1) ? 1'b1:1'b0;  //shcp_clk negedge

//-----------------------------seg display state----------------------------
reg [4:0] state;
reg stcp_r;
reg seg_data_r;
always @ (posedge i_clk or negedge i_rst_n)
begin
    if (!i_rst_n) begin
        state <= 1'b0;
        stcp_r <= 1'b1;
        seg_data_r <= 1'b0;
    end
    else if (shcp_flag) begin
        case (state)
            //千位
            5'd0: begin
        	seg_data_r <= digit[3][7];
        	stcp_r <= 1'b1;							
        	state <= state + 1'b1;
            end
            5'd1: begin
        	seg_data_r <= digit[3][6];
        	state <= state + 1'b1;
            end
            5'd2: begin
        	seg_data_r <= digit[3][5];
        	state <= state + 1'b1;
            end
            5'd3: begin
        	seg_data_r <= digit[3][4];
        	state <= state + 1'b1;
            end
            5'd4: begin
        	seg_data_r <= digit[3][3];
        	state <= state + 1'b1;
            end
            5'd5: begin
        	seg_data_r <= digit[3][2];
        	state <= state + 1'b1;
            end
            5'd6: begin
        	seg_data_r <= digit[3][1];
        	state <= state + 1'b1;
            end
            5'd7: begin
        	seg_data_r <= digit[3][0];
        	state <= state + 1'b1;
            end
        
            //百位
            5'd8: begin
        	seg_data_r <= digit[2][7];
        	stcp_r <= 1'b1;							
        	state <= state + 1'b1;
            end
            5'd9: begin
        	seg_data_r <= digit[2][6];
        	state <= state + 1'b1;
            end
            5'd10: begin
        	seg_data_r <= digit[2][5];
        	state <= state + 1'b1;
            end
            5'd11: begin
        	seg_data_r <= digit[2][4];
        	state <= state + 1'b1;
            end
            5'd12: begin
        	seg_data_r <= digit[2][3];
        	state <= state + 1'b1;
            end
            5'd13: begin
        	seg_data_r <= digit[2][2];
        	state <= state + 1'b1;
            end
            5'd14: begin
        	seg_data_r <= digit[2][1];
        	state <= state + 1'b1;
            end
            5'd15: begin
        	seg_data_r <= digit[2][0];
        	state <= state + 1'b1;
            end
        
            //十位
            5'd16: begin
        	seg_data_r <= digit[1][7];
                stcp_r <= 1'b1;
        	state <= state + 1'b1;
            end
            5'd17: begin
        	seg_data_r <= digit[1][6];
        	state <= state + 1'b1;
            end
            5'd18: begin
        	seg_data_r <= digit[1][5];
        	state <= state + 1'b1;
            end
            5'd19: begin
        	seg_data_r <= digit[1][4];
        	state <= state + 1'b1;
            end
            5'd20: begin
        	seg_data_r <= digit[1][3];
        	state <= state + 1'b1;
            end
            5'd21: begin
        	seg_data_r <= digit[1][2];
        	state <= state + 1'b1;
            end
            5'd22: begin
        	seg_data_r <= digit[1][1];
        	state <= state + 1'b1;
            end
            5'd23: begin
        	seg_data_r <= digit[1][0];
        	state <= state + 1'b1;
            end
        
            //个位
            5'd24: begin
        	seg_data_r <= digit[0][7];
                stcp_r <= 1'b1;
        	state <= state + 1'b1;
            end
            5'd25: begin
        	seg_data_r <= digit[0][6];
        	state <= state + 1'b1;
            end
            5'd26: begin
        	seg_data_r <= digit[0][5];
        	state <= state + 1'b1;
            end
            5'd27: begin
        	seg_data_r <= digit[0][4];
        	state <= state + 1'b1;
            end
            5'd28: begin
        	seg_data_r <= digit[0][3];
        	state <= state + 1'b1;
            end
            5'd29: begin
        	seg_data_r <= digit[0][2];
        	state <= state + 1'b1;
            end
            5'd30: begin
        	seg_data_r <= digit[0][1];
        	state <= state + 1'b1;
            end
            5'd31: begin
        	seg_data_r <= digit[0][0];
        	stcp_r <= 1'b0;    //latch the all data
        	state <= 1'b0;
            end
            default: state <= 5'bxxxxx;
        endcase	
    end
end

assign o_shcp = shcp_r1;  //output to drive 74HC595 shift reg
assign o_stcp = stcp_r;   //output to drive 74HC595 latch reg
assign o_ds = seg_data_r;
			
endmodule
