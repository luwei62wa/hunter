## This file is a general .xdc for the Han325t Rev. E + Peripheral Board
## To use it in a project:
## - uncomment the lines corresponding to used pins
## - rename the used ports (in each line, after get_ports) according to the top level signal names in the project

# VCCIO = 1.8V/3.3V: bank 12,13,16,17  (jumper VCC_ADJ=3.3V)
# VCCIO = 3.3V:      bank 18
# VCCIO = 1.8V:      bank 32,33,34
# VCCIO = 2.5V:      bank 14,15,0

## Clock signal

#set_property -dict { PACKAGE_PIN AF18  IOSTANDARD LVCMOS18 } [get_ports { clk100_i }]; #IO_L11P_T1_SRCC_32 Sch=USER_CLOCKCA@@100MHz@1.8V
#set_property -dict { PACKAGE_PIN AF17  IOSTANDARD LVCMOS18 } [get_ports { clk100_i }]; #IO_L12P_T1_MRCC_32 Sch=USER_CLOCKCB@100MHz@1.8V
set_property -dict { PACKAGE_PIN G13   IOSTANDARD LVCMOS33 } [get_ports { clk100_i }]; #IO_L12P_T1_MRCC_18 Sch=USER_CLOCKBB@100MHz@3.3V
#set_property -dict { PACKAGE_PIN K28   IOSTANDARD LVCMOS25 } [get_ports { clk100_i }]; #IO_L13P_T2_MRCC_15 Sch=USER_CLOCKAB@100MHz@2.5V
#set_property -dict { PACKAGE_PIN R24   IOSTANDARD LVCMOS25 } [get_ports { clk100_i }]; #IO_L3N_T0_DQS_EMCCLK_14 Sch=SYSTEM_CLOCK@60MHz@2.5V
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports { clk100_i }];

##SOC JTAG on mother board
set_property -dict { PACKAGE_PIN J12   IOSTANDARD LVCMOS33 } [get_ports { mtms }];  #IO_L8N_T1_18     Sch=TMS
set_property -dict { PACKAGE_PIN J16   IOSTANDARD LVCMOS33 } [get_ports { mtdo }];  #IO_L9P_T1_DQS_18 Sch=TDO
set_property -dict { PACKAGE_PIN H16   IOSTANDARD LVCMOS33 } [get_ports { mtdi }];  #IO_L9N_T1_DQS_18 Sch=TDI
set_property -dict { PACKAGE_PIN H11   IOSTANDARD LVCMOS33 } [get_ports { mtck }];  #IO_L10P_T1_18    Sch=TCLK
set_property -dict { PACKAGE_PIN H12   IOSTANDARD LVCMOS33 } [get_ports { mtrst }]; #IO_L10N_T1_18    Sch=TRST
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets mtck_IBUF]

##SOC JTAG
set_property -dict { PACKAGE_PIN D26   IOSTANDARD LVCMOS33 } [get_ports { tms }];  #IO_L11P_T1_SRCC_16  Sch=TMS=HSMC.145=HSMC_TX_D_P10
set_property -dict { PACKAGE_PIN C29   IOSTANDARD LVCMOS33 } [get_ports { tdo }];  #IO_L15P_T2_DQS_16   Sch=TDO=HSMC.155=HSMC_RX_D_P2
set_property -dict { PACKAGE_PIN E26   IOSTANDARD LVCMOS33 } [get_ports { tdi }];  #IO_L5N_T0_16        Sch=TDI=HSMC.143=HSMC_TX_D_N4
set_property -dict { PACKAGE_PIN E28   IOSTANDARD LVCMOS33 } [get_ports { tck }];  #IO_L14P_T2_SRCC_16  Sch=TCK=HSMC.149=HSMC_RX_D_P1
set_property -dict { PACKAGE_PIN D28   IOSTANDARD LVCMOS33 } [get_ports { rtck }]; #IO_L14N_T2_SRCC_16  Sch=RTCK=HSMC.151=HSMC_RX_D_N1
set_property -dict { PACKAGE_PIN D24   IOSTANDARD LVCMOS33 } [get_ports { trst }]; #IO_L4N_T0_16        Sch=TRST=HSMC.139=HSMC_TX_D_N3
set_property -dict { PACKAGE_PIN E30   IOSTANDARD LVCMOS33 } [get_ports { srst }]; #IO_L18N_T2_16       Sch=SRST=HSMC.157=HSMC_RX_D_N5

##Switches

set_property -dict { PACKAGE_PIN H25   IOSTANDARD LVCMOS33 } [get_ports { sw[3] }]; #IO_L19N_T3_VREF_16 Sch=Switch.4=GPIO_13=HSMC.86=HSMC_RX_D_N6
set_property -dict { PACKAGE_PIN H24   IOSTANDARD LVCMOS33 } [get_ports { sw[2] }]; #IO_L19P_T3_16      Sch=Switch.3=GPIO_14=HSMC.84=HSMC_RX_D_P6
set_property -dict { PACKAGE_PIN E30   IOSTANDARD LVCMOS33 } [get_ports { sw[1] }]; #IO_L18N_T2_16      Sch=Switch.2=GPIO_15=HSMC.80=HSMC_RX_D_N5
set_property -dict { PACKAGE_PIN E29   IOSTANDARD LVCMOS33 } [get_ports { sw[0] }]; #IO_L18P_T2_16      Sch=Switch.1=GPIO_16=HSMC.78=HSMC_RX_D_P5

##Buttons on mother board

#set_property -dict { PACKAGE_PIN L30   IOSTANDARD LVCMOS25 } [get_ports { key[0] }]; #IO_L17N_T2_16 Sch=IO_3V3_2=KEY_IN1
#set_property -dict { PACKAGE_PIN J28   IOSTANDARD LVCMOS25 } [get_ports { key[1] }]; #IO_L17P_T2_16 Sch=IO_3V3_1=KEY_IN2
set_property -dict { PACKAGE_PIN K30   IOSTANDARD LVCMOS25 } [get_ports { sys_rst_n }]; #IO_L16N_T2_16 Sch=IO_3V3_3=KEY_IN3=SystemReset
#set_property -dict { PACKAGE_PIN K26   IOSTANDARD LVCMOS25 } [get_ports { key[3] }]; #IO_L16P_T2_16 Sch=IO_3V3_4=FPGA_RAM_RESET

##Segmented LEDs with 74AHC595 driver on mother board
set_property -dict { PACKAGE_PIN J13   IOSTANDARD LVCMOS33 } [get_ports { led_sdin }]; #IO_L4N_T0_18 Sch=FPGA_LED_SDIN
set_property -dict { PACKAGE_PIN K14   IOSTANDARD LVCMOS33 } [get_ports { led_shcp }]; #IO_L5P_T0_18 Sch=FPGA_LED_SHCP
set_property -dict { PACKAGE_PIN J14   IOSTANDARD LVCMOS33 } [get_ports { led_stcp }]; #IO_L5N_T0_18 Sch=FPGA_LED_STCP

##LEDs on mother board

set_property -dict { PACKAGE_PIN J26   IOSTANDARD LVCMOS25 } [get_ports { mled[0] }]; #IO_L10N_T1_AD4N_15       Sch=IO_3V3_4=D18
set_property -dict { PACKAGE_PIN L26   IOSTANDARD LVCMOS25 } [get_ports { mled[1] }]; #IO_L11P_T1_SRCC_AD12P_15 Sch=IO_3V3_5=D19
set_property -dict { PACKAGE_PIN L27   IOSTANDARD LVCMOS25 } [get_ports { mled[2] }]; #IO_L11N_T1_SRCC_AD12N_15 Sch=IO_3V3_6=D20
set_property -dict { PACKAGE_PIN M28   IOSTANDARD LVCMOS25 } [get_ports { mled[3] }]; #IO_L14P_T2_SRCC_15       Sch=IO_3V3_7=D21

##LEDs

set_property -dict { PACKAGE_PIN H21   IOSTANDARD LVCMOS33 } [get_ports { led[0] }]; #IO_L7P_T1_17      Sch=LED303=GPIO_1=HSMC.90=HSMC_TX_D_P18
set_property -dict { PACKAGE_PIN H22   IOSTANDARD LVCMOS33 } [get_ports { led[1] }]; #IO_L7N_T1_17      Sch=LED304=GPIO_2=HSMC.92=HSMC_TX_D_N18
set_property -dict { PACKAGE_PIN G23   IOSTANDARD LVCMOS33 } [get_ports { led[2] }]; #IO_L6P_T0_16      Sch=LED305=GPIO_3=HSMC.96=HSMC_TX_D_P5
set_property -dict { PACKAGE_PIN G24   IOSTANDARD LVCMOS33 } [get_ports { led[3] }]; #IO_L6N_T0_VREF_16 Sch=LED306=GPIO_4=HSMC.98=HSMC_TX_D_N5
set_property -dict { PACKAGE_PIN A23   IOSTANDARD LVCMOS33 } [get_ports { led[4] }]; #IO_L1N_T0_16      Sch=LED307=GPIO_5=HSMC.89=HSMC_TX_D_N0
set_property -dict { PACKAGE_PIN A25   IOSTANDARD LVCMOS33 } [get_ports { led[5] }]; #IO_L10P_T1_16     Sch=LED308=GPIO_6=HSMC.91=HSMC_TX_D_P9
set_property -dict { PACKAGE_PIN A27   IOSTANDARD LVCMOS33 } [get_ports { led[6] }]; #IO_L7N_T1_16      Sch=LED309=GPIO_7=HSMC.95=HSMC_TX_D_N6
set_property -dict { PACKAGE_PIN G25   IOSTANDARD LVCMOS33 } [get_ports { led[7] }]; #IO_25_16          Sch=LED310=GPIO_8=HSMC.97=HSMC_IO_2

##Buttons

set_property -dict { PACKAGE_PIN A18   IOSTANDARD LVCMOS33 } [get_ports { btn[0] }]; #IO_L22N_T3_17      Sch=KEY1=GPIO_9=HSMC.74=HSMC_RX_D_N21
set_property -dict { PACKAGE_PIN B18   IOSTANDARD LVCMOS33 } [get_ports { btn[1] }]; #IO_L22P_T3_17      Sch=KEY2=GPIO_10=HSMC.72=HSMC_RX_D_P21
set_property -dict { PACKAGE_PIN D18   IOSTANDARD LVCMOS33 } [get_ports { btn[2] }]; #IO_L13N_T2_MRCC_17 Sch=KEY3=GPIO_11=HSMC.68=HSMC_RX_D_N12
set_property -dict { PACKAGE_PIN A17   IOSTANDARD LVCMOS33 } [get_ports { btn[3] }]; #IO_L20N_T3_17      Sch=KEY4=GPIO_12=HSMC.66=HSMC_RX_D_N19

##P600

set_property -dict { PACKAGE_PIN E24   IOSTANDARD LVCMOS33 } [get_ports { ja[0] }]; #IO_L4P_T0_16       Sch=GPIO_137=HSMC_TX_D_P3
set_property -dict { PACKAGE_PIN F30   IOSTANDARD LVCMOS33 } [get_ports { ja[1] }]; #IO_L22N_T3_16      Sch=GPIO_116=HSMC_RX_D_N9
set_property -dict { PACKAGE_PIN F22   IOSTANDARD LVCMOS33 } [get_ports { ja[2] }]; #IO_L9N_T1_DQS_17   Sch=GPIO_133=HSMC_TX_D_N20
set_property -dict { PACKAGE_PIN G29   IOSTANDARD LVCMOS33 } [get_ports { ja[3] }]; #IO_L22P_T3_16      Sch=GPIO_114=HSMC_RX_D_P9
set_property -dict { PACKAGE_PIN G22   IOSTANDARD LVCMOS33 } [get_ports { ja[4] }]; #IO_L9P_T1_DQS_17   Sch=GPIO_131=HSMC_TX_D_P20
set_property -dict { PACKAGE_PIN F26   IOSTANDARD LVCMOS33 } [get_ports { ja[5] }]; #IO_L5P_T0_16       Sch=GPIO_110=HSMC_TX_D_P4
set_property -dict { PACKAGE_PIN E20   IOSTANDARD LVCMOS33 } [get_ports { ja[6] }]; #IO_L12N_T1_MRCC_17 Sch=GPIO_127=HSMC_TX_D_N23
set_property -dict { PACKAGE_PIN H26   IOSTANDARD LVCMOS33 } [get_ports { ja[7] }]; #IO_L23P_T3_16      Sch=GPIO_108=HSMC_RX_D_P10

set_property -dict { PACKAGE_PIN G19   IOSTANDARD LVCMOS33 } [get_ports { jb[0] }]; #IO_0_17            Sch=GPIO_125=HSMC_IO_3
set_property -dict { PACKAGE_PIN H25   IOSTANDARD LVCMOS33 } [get_ports { jb[1] }]; #IO_L19N_T3_VREF_16 Sch=GPIO_104=HSMC_RX_D_N6
set_property -dict { PACKAGE_PIN J18   IOSTANDARD LVCMOS33 } [get_ports { jb[2] }]; #IO_L1N_T0_17       Sch=GPIO_121=HSMC_TX_D_N12
set_property -dict { PACKAGE_PIN H24   IOSTANDARD LVCMOS33 } [get_ports { jb[3] }]; #IO_L19P_T3_16      Sch=GPIO_102=HSMC_RX_D_P6
set_property -dict { PACKAGE_PIN G17   IOSTANDARD LVCMOS33 } [get_ports { jb[4] }]; #IO_L18P_T2_17      Sch=GPIO_119=HSMC_RX_D_P17
set_property -dict { PACKAGE_PIN G28   IOSTANDARD LVCMOS33 } [get_ports { jb[5] }]; #IO_L20P_T3_16      Sch=GPIO_107=HSMC_RX_D_P7
set_property -dict { PACKAGE_PIN G30   IOSTANDARD LVCMOS33 } [get_ports { jb[6] }]; #IO_L24N_T3_16      Sch=GPIO_115=HSMC_RX_D_N11
set_property -dict { PACKAGE_PIN G27   IOSTANDARD LVCMOS33 } [get_ports { jb[7] }]; #IO_L21P_T3_DQS_16  Sch=GPIO_103=HSMC_RX_D_P8

set_property -dict { PACKAGE_PIN E29   IOSTANDARD LVCMOS33 } [get_ports { jc[0] }]; #IO_L18P_T2_16      Sch=GPIO_113=HSMC_RX_D_P5
set_property -dict { PACKAGE_PIN H27   IOSTANDARD LVCMOS33 } [get_ports { jc[1] }]; #IO_L23N_T3_16      Sch=GPIO_101=HSMC_RX_D_N10
set_property -dict { PACKAGE_PIN F28   IOSTANDARD LVCMOS33 } [get_ports { jc[2] }]; #IO_L20N_T3_16      Sch=GPIO_109=HSMC_RX_D_N7
set_property -dict { PACKAGE_PIN C21   IOSTANDARD LVCMOS33 } [get_ports { jc[3] }]; #IO_L8N_T1_17       Sch=GPIO_85=HSMC_TX_D_N19

##P700

set_property -dict { PACKAGE_PIN F17   IOSTANDARD LVCMOS33 } [get_ports { jc[4] }]; #IO_L18N_T2_17      Sch=GPIO_F1=HSMC.1=HSMC_RX_D_N17 (near Switch)
set_property -dict { PACKAGE_PIN E18   IOSTANDARD LVCMOS33 } [get_ports { jc[5] }]; #IO_25_17           Sch=GPIO_F4=HSMC.4=HSMC_IO_4
set_property -dict { PACKAGE_PIN F18   IOSTANDARD LVCMOS33 } [get_ports { jc[6] }]; #IO_L16N_T2_17      Sch=GPIO_F2=HSMC.2=HSMC_RX_D_N15
set_property -dict { PACKAGE_PIN D17   IOSTANDARD LVCMOS33 } [get_ports { jc[7] }]; #IO_L13P_T2_MRCC_17 Sch=GPIO_F3=HSMC.3=HSMC_RX_D_P12
#set_property -dict { PACKAGE_PIN C16   IOSTANDARD LVCMOS33 } [get_ports { jd[0] }]; #IO_L15N_T2_DQS_17 Sch=GPIO_50=HSMC.50=HSMC_RX_D_N14

##USB-UART Interface

set_property -dict { PACKAGE_PIN C17   IOSTANDARD LVCMOS33 } [get_ports { uart1_txd_i }]; #IO_L17P_T2_17 Sch=RXD1=HSMC.60=HSMC_RX_D_P16 (TBD: swap to meet sch P502=VDD/GND/TXD1/RXD1/CTS1/RTS1)
set_property -dict { PACKAGE_PIN A16   IOSTANDARD LVCMOS33 } [get_ports { uart1_rxd_o }]; #IO_L20P_T3_17 Sch=TXD1=HSMC.54=HSMC_RX_D_P19

set_property -dict { PACKAGE_PIN K20   IOSTANDARD LVCMOS33 } [get_ports { uart_txd_i }]; #IO_L6N_T0_VREF_17  Sch=RXD0=HSMC.44=HSMC_TX_D_N17
set_property -dict { PACKAGE_PIN F20   IOSTANDARD LVCMOS33 } [get_ports { uart_rxd_o }]; #IO_L12P_T1_MRCC_17 Sch=TXD0=HSMC.42=HSMC_TX_D_P23

## ChipKit SPI

#set_property -dict { PACKAGE_PIN G1    IOSTANDARD LVCMOS33 } [get_ports { ck_miso }]; #IO_L17N_T2_35 Sch=ck_miso
#set_property -dict { PACKAGE_PIN H1    IOSTANDARD LVCMOS33 } [get_ports { ck_mosi }]; #IO_L17P_T2_35 Sch=ck_mosi
#set_property -dict { PACKAGE_PIN F1    IOSTANDARD LVCMOS33 } [get_ports { ck_sck }]; #IO_L18P_T2_35 Sch=ck_sck
#set_property -dict { PACKAGE_PIN C1    IOSTANDARD LVCMOS33 } [get_ports { ck_ss }]; #IO_L16N_T2_35 Sch=ck_ss

## ChipKit I2C

#set_property -dict { PACKAGE_PIN L18   IOSTANDARD LVCMOS33 } [get_ports { ck_scl }]; #IO_L4P_T0_D04_14 Sch=ck_scl
#set_property -dict { PACKAGE_PIN M18   IOSTANDARD LVCMOS33 } [get_ports { ck_sda }]; #IO_L4N_T0_D05_14 Sch=ck_sda
#set_property -dict { PACKAGE_PIN A14   IOSTANDARD LVCMOS33 } [get_ports { scl_pup }]; #IO_L9N_T1_DQS_AD3N_15 Sch=scl_pup
#set_property -dict { PACKAGE_PIN A13   IOSTANDARD LVCMOS33 } [get_ports { sda_pup }]; #IO_L9P_T1_DQS_AD3P_15 Sch=sda_pup

##Quad SPI Flash

set_property -dict { PACKAGE_PIN F25   IOSTANDARD LVCMOS33 } [get_ports { flash_sck_o }]; #IO_L3P_T0_DQS_16  Sch=NOR_SCLK=HSMC.144=HSMC_TX_D_P2
set_property -dict { PACKAGE_PIN F23   IOSTANDARD LVCMOS33 } [get_ports { flash_cs_o }]; #IO_0_16            Sch=NOR_CE=HSMC.138=HSMC_IO_1
set_property -dict { PACKAGE_PIN E25   IOSTANDARD LVCMOS33 } [get_ports { flash_si_o }]; #IO_L3N_T0_DQS_16   Sch=NOR_MOSI=HSMC.146=HSMC_TX_D_N2
set_property -dict { PACKAGE_PIN E23   IOSTANDARD LVCMOS33 } [get_ports { flash_so_i }]; #IO_L2P_T0_16       Sch=NOR_MISO=HSMC.140=HSMC_TX_D_P1
#set_property -dict { PACKAGE_PIN E21   IOSTANDARD LVCMOS33 } [get_ports { qspi_dq[2] }]; #IO_L11N_T1_SRCC_17 Sch=NOR_WP=HSMC.134=HSMC_TX_D_N22
#set_property -dict { PACKAGE_PIN G20   IOSTANDARD LVCMOS33 } [get_ports { qspi_dq[3] }]; #IO_L2N_T0_17       Sch=NOR_HOLD=HSMC.132=HSMC_TX_D_N13
