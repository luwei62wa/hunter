// @copyright Copyright (c) 2022 OnMicro Corp.
// @brief     T-Head's E906(rv32imafc) SoC w/ JTAG on OnMicro han325t FPGA top file.
// @author    wei.lu@onmicro.com.cn
// @license   SPDX-License-Identifier: Apache-2.0

`define FPGA

module fpga_top
(
     input        clk100_i
    ,input        sys_rst_n

    // UART
    ,input        uart_txd_i
    ,output       uart_rxd_o

    // GPIO
    ,inout  [7:0] ja

    // JTAG
    ,inout        tck
    ,inout        tms
    ,input        trst
    ,input        tdi
    ,output       tdo

    // LEDs
    ,output [7:0] led
    ,output [3:0] mled
    ,output       led_shcp
    ,output       led_stcp
    ,output       led_sdin
);

//-----------------------------------------------------------------
// PLL
//-----------------------------------------------------------------
wire sys_clk;

kintex7_pll
u_pll
(
    .clkref_i(clk100_i),
    .clkout0_o(sys_clk)
);

//-----------------------------------------------------------------
// SoC
//-----------------------------------------------------------------
soc
u_soc(
     .b_pad_gpio_porta(ja)
    ,.i_pad_clk(sys_clk)
    ,.i_pad_jtg_nrst_b(sys_rst_n)
    ,.i_pad_jtg_tclk(tck)
    ,.i_pad_jtg_tms(tms)
    ,.i_pad_jtg_trst_b(trst)
    ,.i_pad_rst_b(sys_rst_n)
    ,.i_pad_uart0_sin(uart_txd_i)
    ,.o_pad_uart0_sout(uart_rxd_o)
);

//-----------------------------------------------------------------
// LED indication
//-----------------------------------------------------------------
led_toggle
#(
    .PERIOD_CNT(50000000)
)
u_led_0
(
     .clk_i(sys_clk)
    ,.rst_i(sys_rst_n)

    ,.led_o(mled[3:2])
);

seg_595
u_led_7seg_0
(
     .i_clk(sys_clk)
    ,.i_rst_n(sys_rst_n)
    ,.i_data(16'hE902)
    ,.o_shcp(led_shcp)
    ,.o_stcp(led_stcp)
    ,.o_ds(led_sdin)
);

endmodule
