# hunter

#### 介绍
收集开源的CPU/SOC，并搭建统一的Verilog Simulator和FPGA验证平台，能加载执行同一个ELF程序。
目前支持的CPU有OpenMSP430, UltraEmbedded RISC-V SoC, T-Head E902, E906;
支持的Simulator有iverilog, verilator;
支持的FPGA综合工具是Xilinx的Vivado。

#### 软件架构
软件目录树组织如下：
--{hunter}
  +--asic                  - TBD.
  +--ip/                   - Verilog source files.
  |  +--cpu/               - Verilog source of CPU modules.
  |  +--soc/               - Verilog source of System on Chip.
  |  +--bus/               - TBD, the shared bus IPs.
  |  +--peripheral/        - TBD, the shared peripheral IPs.
  |  +--memory/            - TBD, the shared memory IPs.
  |  +--wireless/          - TBD, exBLE IP.
  |  +--motor/             - TBD, motor algorithm accelerater IP.
  +--tb/                   - Verilog testbench files.
  |  +--common/            - The common build rules of testbench.
  |  +--tb_openmsp430/     - OpenMSP430 testbench.
  |  +--tb_ue_soc/         - UltraEmbedded's SoC testbench: RISC-V or Cortex-M0.
  |  +--tb_e902/           - T-Head's E902 testbench.
  |  +--tb_e906/           - T-Head's E906 testbench.
  +--fpga/                 - FPGA image generation files.
  |  +--common/            - The common generate rules of FPGA.
  |  +--han325t/           - The common sources and constrains of OnMicro's han325t FPGA devboard.
  |  +--han325t_openmsp430/- Generate FPGA image for OpenMSP430.
  |  +--han325t_ue_rv32im/ - Generate FPGA image for UltraEmbedded SoC with RISC-V core.
  |  +--han325t_e902/      - Generate FPGA image for E902.
  |  +--han325t_e906/      - Generate FPGA image for E906.
  +--prebuilt/             - The prebuilt C programs, including helloworld, coremark etc.
  
#### 安装教程

以下都是Windows PC环境的安装或者编译。

1.  如果SIM=iverilog，可安装icarus：
http://bleyer.org/icarus/iverilog-v11-20210204-x64_setup.exe

2.  如果DUMP=on，可安装gtkwave浏览波形：
gtkwave-3.3.100-bin-win32.zip

3.  如果SIM=verilator，可先安装mingw：
Git-2.10.0-64-bit.exe提供一个mingw环境以及基本的命令行工具；
strawberry-perl-5.32.1.1-64bit.msi提供了一个mingw下的gcc；

4. 在mingw里编译verilator.
https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.3.tar.gz
verilator-v4.224.tar.gz
（略）

5. MSP430需要安装
msp430-gcc-9.3.1.11_win64.zip
msp430-gcc-support-files-1.212.zip

以上，在Ubuntu等Linux发布版里，有现成的安装包。

6. RISC-V需要安装
Nuclei's rv_windows_bare_9.21_win32.zip
T-head's Xuantie-900-gcc-elf-newlib-mingw-V2.6.0-20220715.tar.gz

7. FPGA生成需要安装Vivado，E906需要高版本。

#### 使用说明

1.  OpenMSP430跑testbench
$ cd hunter/tb/tb_openmsp430
$ make                                   ;缺省是使用iverilog跑预编译的dhrystone_4mcu.elf
$ make CASE=coremark_v1.0 SIM=verilator  ;使用verilator跑预编译的coremark_v1.0.elf
$ make CASE=src_c SIM=verilator DUMP=on  ;使用verilator跑gcc现场编译出来的C工程，输出波形文件

2.  E902跑testbench
$ cd hunter/tb/tb_e902
$ make CASE=coremark_v1.0 SIM=verilator  ;使用verilator跑预编译的coremark_v1.0.elf
$ make CASE=src_c SIM=iverilog           ;使用iverilog跑gcc现场编译出来的C工程

3.  E906生成FPGA image
$ cd hunter/fpga/han325t_e906
$ make                                   ;第一次生成vivado工程文件失败
$ cp create_project.tcl project/
$ make                                   ;第二次生成han325t_e906.bit,可下载至han325t FPGA

  以上没有列出hunter所有的用法，OpenMSP430,RV32IM,E902,E906的testbench和FPGA都测试通过。

  E906支持私有的2线JTAG协议，不过需要CKlink仿真器；
  E906也支持RISC-V标准5线JTAG协议，使用JLink都可以连接上：
$ JLinkARM_V782\JLinkGDBServerCL.exe -device risc-v -if JTAG -noreset -noLocalhostOnly
$ riscv64-unknown-elf-gdb --annotate=0 -i=mi prebuilt/e906/coremark_v1.0.elf
(gdb) target remote localhost:2331
(gdb) load
(gdb) c
注意：E906也支持Segger RTT输出，但是不支持后台内存访问，所以RTT和GDB不能同时连接。

#### IP来源

1.  svn co http://opencores.org/ocsvn/openmsp430/openmsp430/trunk/ openmsp430
2.  git clone --recursive https://github.com/ultraembedded/riscv_soc.git
3.  git clone https://github.com/T-head-Semi/opene902.git
    git clone https://github.com/T-head-Semi/opene906.git
    git clone https://github.com/T-head-Semi/wujian100_open.git
