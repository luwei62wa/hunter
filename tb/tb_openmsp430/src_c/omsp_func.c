/*===========================================================================*/
/* Copyright (C) 2001 Authors                                                */
/*                                                                           */
/* This source file may be used and distributed without restriction provided */
/* that this copyright statement is not removed from the file and that any   */
/* derivative work contains the original copyright notice and the associated */
/* disclaimer.                                                               */
/*                                                                           */
/* This source file is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU Lesser General Public License as published  */
/* by the Free Software Foundation; either version 2.1 of the License, or    */
/* (at your option) any later version.                                       */
/*                                                                           */
/* This source is distributed in the hope that it will be useful, but WITHOUT*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     */
/* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public       */
/* License for more details.                                                 */
/*                                                                           */
/* You should have received a copy of the GNU Lesser General Public License  */
/* along with this source; if not, write to the Free Software Foundation,    */
/* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA        */
/*                                                                           */
/*===========================================================================*/
/*                             OMSP_FUNC C FILE                              */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Author(s):                                                                */
/*             - Olivier Girard,    olgirard@gmail.com                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* $Rev: 19 $                                                                */
/* $LastChangedBy: olivier.girard $                                          */
/* $LastChangedDate: 2009-08-04 23:47:15 +0200 (Tue, 04 Aug 2009) $          */
/*===========================================================================*/

#include "omsp_system.h"

#define BAUD            434            // 115200  @50.0MHz

static unsigned char uart_inited;
static void uart_init(void)
{
  UART_BAUD = BAUD;
  UART_CTL  = UART_EN | UART_IEN_RX;

  if (UART_BAUD == BAUD) {
    uart_inited = 1;
  } else {
    uart_inited = -1;
  }
}

//--------------------------------------------------//
//                 putChar function                 //
//            (Send a byte to the Port-1)           //
//--------------------------------------------------//
//int putchar (int txdata) {
int tty_putc (int txdata) {

  if (0 == uart_inited) {
    uart_init();
  }

  if (1 == uart_inited) {
    /* FPGA or ASIC version. */
    // Wait until the TX buffer is not full
    while (UART_STAT & UART_TX_FULL);

    // Write the output character
    UART_TXD = txdata;
  } else {
    /* Simulation version. */
    // Write the output character to the Port-1
    P1OUT  = txdata;

    // Toggle Port-2[0] to signal new byte
    P2OUT ^= 0x01;
  }

  return 0;
}
