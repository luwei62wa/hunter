/*===========================================================================*/
/* Copyright (C) 2001 Authors                                                */
/*                                                                           */
/* This source file may be used and distributed without restriction provided */
/* that this copyright statement is not removed from the file and that any   */
/* derivative work contains the original copyright notice and the associated */
/* disclaimer.                                                               */
/*                                                                           */
/* This source file is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU Lesser General Public License as published  */
/* by the Free Software Foundation; either version 2.1 of the License, or    */
/* (at your option) any later version.                                       */
/*                                                                           */
/* This source is distributed in the hope that it will be useful, but WITHOUT*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     */
/* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public       */
/* License for more details.                                                 */
/*                                                                           */
/* You should have received a copy of the GNU Lesser General Public License  */
/* along with this source; if not, write to the Free Software Foundation,    */
/* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA        */
/*                                                                           */
/*===========================================================================*/
/*                                 SANDBOX                                   */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Author(s):                                                                */
/*             - Olivier Girard,    olgirard@gmail.com                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* $Rev: 19 $                                                                */
/* $LastChangedBy: olivier.girard $                                          */
/* $LastChangedDate: 2009-08-04 23:47:15 +0200 (Tue, 04 Aug 2009) $          */
/*===========================================================================*/

`ifdef verilator
integer main_cycles;
`endif

initial
   begin
      $display(" ===============================================");
      $display("|                 START SIMULATION              |");
      $display(" ===============================================");
`ifdef verilator
      main_cycles = 0;
`else
      // Disable automatic DMA verification
      #10;
      dma_verif_on = 0;

      repeat(5) @(posedge mclk);
`endif
      stimulus_done = 0;

      //---------------------------------------
      // Check CPU configuration
      //---------------------------------------
      if ((`PMEM_SIZE !== 49152) || (`DMEM_SIZE !== 10240))
        begin
           $display(" ===============================================");
           $display("|               SIMULATION ERROR                |");
           $display("|                                               |");
           $display("|  Core must be configured for:                 |");
           $display("|               - 48kB program memory           |");
           $display("|               - 10kB data memory              |");
           $display(" ===============================================");
           $finish;
        end

      //---------------------------------------
      // Generate stimulus
      //---------------------------------------

`ifndef verilator
      repeat(1000) @(posedge mclk);
      p1_din = 8'h01;
      repeat(10) @(posedge mclk);
      p1_din = 8'h00;
      repeat(1000) @(posedge mclk);
      p1_din = 8'h01;
      repeat(10) @(posedge mclk);
      p1_din = 8'h00;
      repeat(1000) @(posedge mclk);

      stimulus_done = 1;

      $display(" ===============================================");
      $display("|               SIMULATION DONE                 |");
      $display("|       (stopped through verilog stimulus)      |");
      $display(" ===============================================");
      $finish;
`endif

   end

`ifdef verilator
always @ (posedge mclk)
begin
    main_cycles = main_cycles + 1;
    if (main_cycles > 1000 && main_cycles < 1010) p1_din = 8'h01;
    else if (main_cycles > 1010 && main_cycles < 2010) p1_din = 8'h00;
    else if (main_cycles > 2010 && main_cycles < 2020) p1_din = 8'h01;
    else if (main_cycles > 2020) p1_din = 8'h00;
    if (main_cycles > 3020) begin
        stimulus_done = 1;
        $display(" ===============================================");
        $display("|               SIMULATION DONE                 |");
        $display("|       (stopped through verilog stimulus)      |");
        $display(" ===============================================");
        $finish;
    end
end
`endif

// Display stuff from the C-program
always @ (posedge p2_dout[0] or negedge p2_dout[0])
begin
    $write("%c", p1_dout);
    $fflush();
end
