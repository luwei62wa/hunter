/*===========================================================================*/
/* Copyright (C) 2001 Authors                                                */
/*                                                                           */
/* This source file may be used and distributed without restriction provided */
/* that this copyright statement is not removed from the file and that any   */
/* derivative work contains the original copyright notice and the associated */
/* disclaimer.                                                               */
/*                                                                           */
/* This source file is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU Lesser General Public License as published  */
/* by the Free Software Foundation; either version 2.1 of the License, or    */
/* (at your option) any later version.                                       */
/*                                                                           */
/* This source is distributed in the hope that it will be useful, but WITHOUT*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     */
/* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public       */
/* License for more details.                                                 */
/*                                                                           */
/* You should have received a copy of the GNU Lesser General Public License  */
/* along with this source; if not, write to the Free Software Foundation,    */
/* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA        */
/*                                                                           */
/*===========================================================================*/
/*                                 COREMARK                                  */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Author(s):                                                                */
/*             - Olivier Girard,    olgirard@gmail.com                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* $Rev: 19 $                                                                */
/* $LastChangedBy: olivier.girard $                                          */
/* $LastChangedDate: 2009-08-04 23:47:15 +0200 (Tue, 04 Aug 2009) $          */
/*===========================================================================*/
`define NO_TIMEOUT

time mclk_start_time, mclk_end_time;
real mclk_period,     mclk_frequency;

time coremark_start_time, coremark_end_time;
real coremark_per_sec;
real coremark_per_mhz;

integer Number_Of_Iterations;

`ifdef verilator
integer main_cycles;
`endif

initial
   begin
      $display(" ===============================================");
      $display("|                 START SIMULATION              |");
      $display(" ===============================================");
`ifdef verilator
      main_cycles = 0;
`else
      // Disable automatic DMA verification
      #10;
      dma_verif_on = 0;

      repeat(5) @(posedge mclk);
`endif
      stimulus_done = 0;

      //---------------------------------------
      // Check CPU configuration
      //---------------------------------------

      if ((`PMEM_SIZE !== 49152) || (`DMEM_SIZE !== 10240))
        begin
           $display(" ===============================================");
           $display("|               SIMULATION ERROR                |");
           $display("|                                               |");
           $display("|  Core must be configured for:                 |");
           $display("|               - 48kB program memory           |");
           $display("|               - 10kB data memory              |");
           $display(" ===============================================");
           $finish;
        end

      // Disable watchdog
      // (only required because RedHat/TI GCC toolchain doesn't disable watchdog properly at startup)
      `ifdef WATCHDOG
        force dut.watchdog_0.wdtcnt   = 16'h0000;
      `endif

      //---------------------------------------
      // Number of benchmark iteration
      // (Must match the C-code value)
      //---------------------------------------

      Number_Of_Iterations = 1;


      //---------------------------------------
      // Measure clock period
      //---------------------------------------
`ifndef verilator
      repeat(100) @(posedge mclk);
      $timeformat(-9, 3, " ns", 10);
      @(posedge mclk);
      mclk_start_time = $time;
      @(posedge mclk);
      mclk_end_time = $time;
      @(posedge mclk);
      mclk_period    = mclk_end_time-mclk_start_time;
      mclk_frequency = 1000/mclk_period;
      $display("\nINFO-VERILOG: openMSP430 System clock frequency %f MHz", mclk_frequency);

      //---------------------------------------
      // Measure CoreMark run time
      //---------------------------------------

      // Detect beginning of run
      @(posedge p2_dout[1]);
      coremark_start_time = $time;
      $timeformat(-3, 3, " ms", 10);
      $display("INFO-VERILOG: CoreMark loop started at %t ", coremark_start_time);
      $display("");
      $display("INFO-VERILOG: Be patient... there could be up to 90ms to simulate");
      $display("");

      // Detect end of run
      @(negedge p2_dout[1]);
      coremark_end_time = $time;
      $timeformat(-3, 3, " ms", 10);
      $display("INFO-VERILOG: Coremark loop ended   at %t ",   coremark_end_time);

      // Compute results
      $timeformat(-9, 3, " ns", 10);
      coremark_per_sec = coremark_end_time - coremark_start_time;
      coremark_per_sec = 1000000000 / coremark_per_sec;
      coremark_per_sec = Number_Of_Iterations*coremark_per_sec;
      coremark_per_mhz = coremark_per_sec / mclk_frequency;

      // Report results
      $display("\INFO-VERILOG: CoreMark ticks      : %d",     {p6_din, p5_din, p4_din, p3_din});
      $display("\INFO-VERILOG: CoreMark per second : %f",     coremark_per_sec);
      $display("\INFO-VERILOG: CoreMark per MHz    : %f\n\n", coremark_per_mhz);

      //---------------------------------------
      // Wait for the end of C-code execution
      //---------------------------------------
      @(posedge p2_dout[7]);

      stimulus_done = 1;

      $display(" ===============================================");
      $display("|               SIMULATION DONE                 |");
      $display("|       (stopped through verilog stimulus)      |");
      $display(" ===============================================");
      $finish;
`endif //  `ifndef verilator

   end

`ifdef verilator
always @ (posedge mclk)
begin
    main_cycles = main_cycles + 1;
end

// Detect beginning of run
always @ (posedge p2_dout[1])
begin
    mclk_frequency = 1000;
    $display("\n[Verilator] openMSP430 System clock frequency %f MHz\n", mclk_frequency);

    coremark_start_time = main_cycles;
    $display("[Verilator] CoreMark loop started at %d ", coremark_start_time);
    $display("");
    $display("[Verilator] Be patient... there could be up to 90ms to simulate");
    $display("");
end

// Detect end of run
always @ (negedge p2_dout[1])
begin
    coremark_end_time = main_cycles;
    $display("[Verilator] Coremark loop ended   at %d ",   coremark_end_time);

    // Compute results
    coremark_per_sec = coremark_end_time - coremark_start_time;
    coremark_per_sec = 1000000000 / coremark_per_sec;
    coremark_per_sec = Number_Of_Iterations*coremark_per_sec;
    coremark_per_mhz = coremark_per_sec / mclk_frequency;

    // Report results
    $display("[Verilator] CoreMark ticks      : %d",     {p6_din, p5_din, p4_din, p3_din});
    $display("[Verilator] CoreMark per second : %f",     coremark_per_sec);
    $display("[Verilator] CoreMark per MHz    : %f\n\n", coremark_per_mhz);
end

//---------------------------------------
// Wait for the end of C-code execution
//---------------------------------------
always @ (posedge p2_dout[7])
begin
    stimulus_done = 1;
    $display(" ===============================================");
    $display("|               SIMULATION DONE                 |");
    $display("|       (stopped through verilog stimulus)      |");
    $display(" ===============================================");
    $finish;
end
`endif //  `ifdef verilator

// Display stuff from the C-program
always @ (posedge p2_dout[0] or negedge p2_dout[0])
begin
    $write("%c", p1_dout);
    $fflush();
end

// Display some info to show simulation progress
`ifndef verilator
initial
  begin
     @(posedge p2_dout[1]);
     #1000000;
     while (p2_dout[1])
       begin
	  $display("INFO-VERILOG: Simulated time %t ", $time);
	  #1000000;
       end
  end
`endif //  `ifndef verilator

// Time tick counter
always @(negedge mclk or posedge puc_rst)
  if (puc_rst)         {p6_din, p5_din, p4_din, p3_din} <= 32'h0000_0000;
  else if (p2_dout[1]) {p6_din, p5_din, p4_din, p3_din} <= {p6_din, p5_din, p4_din, p3_din} + 32'h1;
