/*
 * Copyright (C) 2017-2019 Alibaba Group Holding Limited
 */


/******************************************************************************
 * @file     main.c
 * @brief    hello world
 * @version  V1.0
 * @date     17. Jan 2018
 ******************************************************************************/

#include <stdio.h>
#include <math.h>

int main(void)
{
    float pi;

    //Section 1: Hello World!
    printf("[src_c] Hello world!\n");

    //Section 2: Embeded ASM in C 
    int a;
    int b;
    int c;
    a=1;
    b=2;
    c=0;

    asm(
        "mv  x5,%[a]\n"
        "mv  x6,%[b]\n"
        "label_add:"
        "add  %[c],x5,x6\n"
        :[c]"=r"(c)
        :[a]"r"(a),[b]"r"(b)
        :"x5","x6"
        );

    /* rv32imafc: single precision FPU. */
    pi = 2 * acosf(0.0+c-3*a);
    printf("[src_c] PI=%.8f\n", pi);

    if (c == 3)
        printf("[src_c] PASS\n");
    else
        printf("[src_c] FAIL\n");

    return 0;
}
