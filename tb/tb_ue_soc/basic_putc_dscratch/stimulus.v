// @copyright Copyright (c) 2022 OnMicro Corp.
// @brief     Test case basic.elf specific stimulus file.
// @author    wei.lu@onmicro.com.cn
// @license   SPDX-License-Identifier: Apache-2.0
initial
begin
    //---------------------------------------
    // test case specific stimulus
    //---------------------------------------

    //basic_putc_dscratch.elf is linked at 0x2000.
    force u_top.u_cpu.boot_vector_w = 32'h2000;
    /*
     * verilator -cc bug:
     * the above force set cannot pass into u_core.reset_vector_i
     * workaround1: please set riscv_tcm_wrapper.BOOT_VECTOR(32'h2000) in verilog source.
     * workaround2: change riscv_tcp_wrapper.boot_vector_w from wire to reg.
     */
end
