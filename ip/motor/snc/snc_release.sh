#!/bin/sh

rm release.log
rm ../../../../contiki/lib/onmicro/snc/libsnc*.lib

echo "compiling libsnc_Cortex-M0+_ARMCC.lib"
make clean 1>>release.log 2>>release.log
make CPU=Cortex-M0+ 1>>release.log 2>>release.log
cp build/libsnc_Cortex-M0+_ARMCC.lib ../../../../contiki/lib/onmicro/snc/
cp build/libsnc_Cortex-M0+_ARMCC.lib ../../../../contiki/lib/onmicro/snc/libsnc_Cortex-M0_ARMCC.lib

echo "compiling libsnc_Cortex-M3_ARMCC.lib"
make clean 1>>release.log 2>>release.log
make CPU=Cortex-M3 1>>release.log 2>>release.log
cp build/libsnc_Cortex-M3_ARMCC.lib ../../../../contiki/lib/onmicro/snc/

echo "compiling libsnc_Cortex-M4_ARMCC.lib"
make clean 1>>release.log 2>>release.log
make CPU=Cortex-M4 1>>release.log 2>>release.log
cp build/libsnc_Cortex-M4_ARMCC.lib ../../../../contiki/lib/onmicro/snc/

echo "releasing to contiki"
cp src_c/snc_api.h ../../../../contiki/lib/onmicro/snc/
arm-none-eabi-strip -S ../../../../contiki/lib/onmicro/snc/libsnc_Cortex-M0_ARMCC.lib
arm-none-eabi-strip -S ../../../../contiki/lib/onmicro/snc/libsnc_Cortex-M0+_ARMCC.lib
arm-none-eabi-strip -S ../../../../contiki/lib/onmicro/snc/libsnc_Cortex-M3_ARMCC.lib
arm-none-eabi-strip -S ../../../../contiki/lib/onmicro/snc/libsnc_Cortex-M4_ARMCC.lib
svn diff ../../../../contiki/lib/onmicro/snc/
