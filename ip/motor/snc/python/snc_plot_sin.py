# -*- coding: utf-8 -*-
# @copyright Copyright (c) 2022 OnMicro Corp.
# @brief     SNC Field Oriented Control algorithm test.
# @author    wei.lu@onmicro.com.cn
# @license   SPDX-License-Identifier: Apache-2.0

import wave
import pylab as plt
import matplotlib.patches as pat
import numpy as np
import scipy.stats as stats

if __name__ == "__main__":
    import os,sys
    if not (len(sys.argv)>1 and os.path.isfile(sys.argv[1])):
        print('usage: {} sin.txt'.format(sys.argv[0]))
        sys.exit(1)

    angle = []
    sin = []
    cos = []

    # Read the output of snc_utest.c and plot its sin.
    fh = open(sys.argv[1], "r")
    for row in fh:
        row = row.split(' ')
        angle.append(int(row[0]))
        sin.append(int(row[1]))
        cos.append(int(row[2]))
    fh.close()

    plt.xlabel('angle (radius)', fontsize = 12)
    plt.ylabel('sin', fontsize = 12)
    plt.title(os.path.basename(sys.argv[1]) + ' waveform', fontsize = 20)

    #plt.bar(angle, sin, color = 'r', lable = 'sin')

    #plt.yticks(sin)
    plt.plot(angle, sin, marker = '.', color = 'g', label = 'sin')
    #plt.yticks(cos)
    plt.plot(angle, cos, marker = '*', color = 'r', label = 'cos')

    plt.grid()
    plt.legend()
    plt.show()
