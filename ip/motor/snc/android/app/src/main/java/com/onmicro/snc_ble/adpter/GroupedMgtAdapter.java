package com.onmicro.snc_ble.adpter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.models.GroupModel;

/**
 * @author peng
 */
public class GroupedMgtAdapter extends BaseQuickAdapter<GroupModel, BaseViewHolder> {
    public GroupedMgtAdapter() {
        super(R.layout.row_grouped_mgt);
    }

    @Override
    protected void convert(BaseViewHolder paramBaseViewHolder, GroupModel paramGroupModel) {
        paramBaseViewHolder.setGone(R.id.editView, paramGroupModel.getSpread());
        paramBaseViewHolder.addOnClickListener(R.id.changeButton);
        paramBaseViewHolder.addOnClickListener(R.id.deleteButton);
        paramBaseViewHolder.setText(R.id.title_text, paramGroupModel.getGroupName());
        if (paramGroupModel.getSpread()) {
            paramBaseViewHolder.setImageResource(R.id.imageview, R.mipmap.grouped_manage_up_pull);
            return;
        }
        paramBaseViewHolder.setImageResource(R.id.imageview, R.mipmap.grouped_manage_drop_down);
    }
}
