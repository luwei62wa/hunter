package com.onmicro.snc_ble.core.bluetooth;

import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.onmicro.snc_ble.app.App;
import com.onmicro.snc_ble.common.PreferenceUtils;

import java.io.PrintStream;

/**
 * @author PengYaQuan
 * @create 2019-10-31 19:55
 * @Describe
 */
public class BluetoothManager {

    private static BluetoothManager instance;
    private static char[] mAddress;

    private static BeaconTransmitter mBeaconTransmitter;

    private String groupId;

    private Handler mHandler = null;

    private long mRecordTime;

    private int mSN = 0;

    private char[] mUUID;

    private int mValue = 0;

    private int mValue1 = 0;

    private BluetoothManager() {
        String str2 = PreferenceUtils.getValue(App.getInstance(), "ANDROID_ID");
        if (str2.length() == 0) {
            str2 = Settings.Secure.getString(App.getInstance().getContentResolver(), "android_id");
            String stringBuilder = str2 + Build.SERIAL;
            str2 = stringBuilder;
            if (str2.length() < 3) {
                str2 = "FFFF";
            } else {
                str2 = str2.substring(0, 4);
            }
            PreferenceUtils.putValue(App.getInstance(), "ANDROID_ID", str2);
        }
        int i = Integer.valueOf(str2, 16);
        this.mUUID = new char[]{(char) ((0xFF00 & i) >> 8), (char) (i & 0xFF)};
        mAddress = new char[] { (char)0xaa, (char)0x55, (char)0xcc };
        this.mHandler = new Handler();
    }


    public static BluetoothManager getInstance() {
        if (instance == null) {
            synchronized (BluetoothManager.class) {
                if (instance == null) {
                    instance = new BluetoothManager();
                    mBeaconTransmitter = new BeaconTransmitter(App.getInstance());
                }
            }
        }
        return instance;
    }

    public static String hexString(char[] paramArrayOfChar) {
        StringBuilder stringBuffer = new StringBuilder();
        for (byte b = 0; b < paramArrayOfChar.length; b++) {
            stringBuffer.append(String.format("%02X ", (byte) paramArrayOfChar[b]));
        }
        return stringBuffer.toString();
    }

    private void startAdvertising(char paramChar, final char[] blePayload) {
        String str;
        if (groupId == null) {
            groupId = "FF";
        }
        if (groupId.length() == 0) {
            str = "FF";
        } else {
            str = this.groupId;
        }
        int i = Integer.valueOf(str, 16);
        if (mSN >= 255) {
            mSN = 0;
        }
        mSN++;
        PrintStream printStream2 = System.out;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("原始数据 -");
        stringBuilder.append(mSN);
        printStream2.println(stringBuilder.toString());
        char[] arrayOfChar = new char[8];
        arrayOfChar[0] = this.mUUID[0];
        arrayOfChar[1] = blePayload[0];
        arrayOfChar[2] = (char) i;
        arrayOfChar[3] = blePayload[1];
        arrayOfChar[4] = paramChar;
        arrayOfChar[5] = this.mUUID[1];
        arrayOfChar[6] = (char) this.mSN;
        arrayOfChar[7] = blePayload[2];
        i = (char) (arrayOfChar[6] ^ arrayOfChar[7]);
        arrayOfChar[0] = (char) (arrayOfChar[0] ^ i);
        arrayOfChar[1] = (char) (arrayOfChar[1] ^ i);
        arrayOfChar[2] = (char) (arrayOfChar[2] ^ i);
        arrayOfChar[3] = (char) (arrayOfChar[3] ^ i);
        arrayOfChar[4] = (char) (arrayOfChar[4] ^ i);
        arrayOfChar[5] = (char) (arrayOfChar[5] ^ i);
        arrayOfChar[6] = (char) (arrayOfChar[6] ^ arrayOfChar[0]);
        arrayOfChar[7] = (char) (arrayOfChar[7] ^ arrayOfChar[1]);
        char[] paramArrayOfChar = new char[16];
        Util.get_rf_payload(mAddress, mAddress.length, arrayOfChar, arrayOfChar.length, paramArrayOfChar);
        for (i = 0; i < 8; i++) {
            paramChar = paramArrayOfChar[i];
            int j = 15 - i;
            paramArrayOfChar[i] = paramArrayOfChar[j];
            paramArrayOfChar[j] = paramChar;
        }
        mBeaconTransmitter.startAdvertising(paramArrayOfChar);
        PrintStream printStream1 = System.out;
        stringBuilder = new StringBuilder();
        stringBuilder.append("sendBytes start");
        stringBuilder.append(hexString(paramArrayOfChar));
        printStream1.println(stringBuilder.toString());
        if (this.mHandler != null) {
            this.mHandler.removeMessages(0);
        }
        this.mHandler.postDelayed(() -> {
            mBeaconTransmitter.stopAdvertising();
            PrintStream printStream = System.out;
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("sendBytes  stop");
            stringBuilder1.append(BluetoothManager.hexString(paramArrayOfChar));
            printStream.println(stringBuilder1.toString());
        }, 3000L);
    }

    private void startAdvertisingInterval(char paramChar, char[] paramArrayOfChar, long paramLong) {
        if (System.currentTimeMillis() - this.mRecordTime > paramLong) {
            this.mRecordTime = System.currentTimeMillis();
            new Thread(() -> BluetoothManager.this.startAdvertising(paramChar, paramArrayOfChar)).start();
        }
    }

    public void clearCode() {
        startAdvertising((char)0xB0, new char[]{0, 0, 0});
    }

    public void close() {
        startAdvertising((char)0xB2, new char[]{0, 0, 0});
    }

    public void open() {
        startAdvertising((char)0xB3, new char[]{0, 0, 0});
    }

    public void pairCode() {
        startAdvertising((char)0xB4, new char[]{0, 0, 0});
    }

    public void setBeganBrightness(int paramInt) {
        startAdvertisingInterval((char)0xB5, new char[]{(char) ((0xFF0000 & paramInt) >> 16), (char) ((0xFF00 & paramInt) >> 8), (char) (paramInt & 0xFF)}, 100L);
    }

    public void setBeganColorTemperature(int paramInt) {
        startAdvertisingInterval((char)0xB7, new char[]{(char) ((0xFF0000 & paramInt) >> 16), (char) ((0xFF00 & paramInt) >> 8), (char) (paramInt & 0xFF)}, 100L);
    }

    public void setCancelTiming(int paramInt1, int paramInt2, int paramInt3) {
        startAdvertising('U', new char[]{(char) paramInt1, (char) paramInt2, (char) paramInt3});
    }

    public void setEndedBrightness(int paramInt) {
        startAdvertising((char)0xB5, new char[]{(char) ((0xFF0000 & paramInt) >> 16), (char) ((0xFF00 & paramInt) >> 8), (char) (paramInt & 0xFF)});
    }

    public void setEndedColorTemperature(int paramInt) {
        startAdvertising((char)0xB7, new char[]{(char) ((0xFF0000 & paramInt) >> 16), (char) ((0xFF00 & paramInt) >> 8), (char) (paramInt & 0xFF)});
    }

    public void setFanIndex(int paramInt) {
        switch (paramInt) {
            case 0:
                startAdvertising((char)0xD8, new char[]{0, 0, 0});
                break;
            case 1:
                startAdvertising((char)0xD2, new char[]{0, 0, 0});
                break;
            case 2:
                startAdvertising((char)0xD1, new char[]{0, 0, 0});
                break;
            default:
                startAdvertising((char)0xD0, new char[]{0, 0, 0});
                break;
        }
    }

    public void setFanMoreModes(int paramInt) {
        switch (paramInt) {
            case 0:
                startAdvertising((char)0xA3, new char[]{255, 255, 0});
                break;
            case 1:
                startAdvertising((char)0xA2, new char[]{0, 255, 0});
                break;
            case 2:
                startAdvertising((char)0xA4, new char[]{123, 123, 0});
                break;
            case 3:
                startAdvertising((char)0xA1, new char[]{25, 25, 0});
                break;
            case 4:
                if (mValue1 >= 2) {
                    mValue1 = 0;
                }
                mValue1++;
                startAdvertising((char)0xA6, new char[]{(char) this.mValue1, '\000', '\000'});
                break;
            case 5:
                break;
            case 6:
                if (mValue >= 3) {
                    mValue = 0;
                }
                mValue++;
                startAdvertising((char)0xA7, new char[]{(char) this.mValue, '\000', '\000'});
                break;
            case 7:
                startAdvertising((char)0xD4, new char[]{0, 0, 0});
                break;
            case 8:
                startAdvertising((char)0xD5, new char[]{0, 0, 0});
                break;
            case 9:
                startAdvertising((char)0xD6, new char[]{0, 0, 0});
                break;
            case 10:
                startAdvertising((char)0xD7, new char[]{0, 0, 0});
                break;
            case 11:
                startAdvertising((char)0xDA, new char[]{0, 0, 0});
                break;
            default:
                startAdvertising((char)0xD9, new char[]{0, 0, 0});
                break;
        }
    }

    public void setGroupId(String paramString) {
        this.groupId = paramString;
    }

    public void setMoreModes(int paramInt) {
        switch (paramInt) {
            case 0:
                startAdvertising((char)0xA3, new char[]{255, 255, 0});
                break;
            case 1:
                startAdvertising((char)0xA2, new char[]{0, 255, 0});
                break;
            case 2:
                startAdvertising((char)0xA4, new char[]{123, 123, 0});
                break;
            case 3:
                startAdvertising((char)0xA1, new char[]{25, 25, 0});
                break;
            case 4:
                if (mValue1 >= 2) {
                    mValue1 = 0;
                }
                mValue1++;
                startAdvertising((char)0xA6, new char[]{(char) this.mValue1, '\000', '\000'});
                break;
            case 5:
                break;
            case 6:
                if (mValue >= 3) {
                    mValue = 0;
                }
                mValue++;
                startAdvertising((char)0xA7, new char[]{(char) this.mValue, '\000', '\000'});
                break;
            case 7:
                startAdvertising((char)0xA8, new char[]{0, 0, 0});
                break;
            case 8:
                startAdvertising((char)0xA9, new char[]{0, 0, 0});
                break;
            default:
                break;
        }
    }

    public void setTiming(int paramInt1, int paramInt2, int paramInt3) {
        startAdvertising((char)0xA5, new char[]{(char) paramInt1, (char) paramInt2, (char) paramInt3});
    }
}
