package com.onmicro.snc_ble.view;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.onmicro.snc_ble.R;

/**
 * @author peng
 */
public class ColorTempCircularSliderView extends ConstraintLayout {
    public static final int COLORTEMP_MAX = 1000;
    private float colorTempValue = 0.0F;
    private OnUpdateColorTempListener mListener;
    private float mOffsetX = 0.0F;
    private PointF mPoint = new PointF(0.0F, 0.0F);
    private ImageView thumbImageView;

    public ColorTempCircularSliderView(Context paramContext) {
        super(paramContext);
    }

    public ColorTempCircularSliderView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initView();
    }

    public ColorTempCircularSliderView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
    }

    private void colorTempSliderEvent(MotionEvent paramMotionEvent) {
        float pivotX = getPivotX();
        float pivotY = getPivotY();
        pivotY = (float) (Math.atan2((paramMotionEvent.getY() - pivotY), (paramMotionEvent.getX() - pivotX)) * 57.29577951308232D + 180.0D + 50.0D);
        pivotX = pivotY;
        if (pivotY > 360.0F)
            pivotX = pivotY - 360.0F;
        if (pivotX <= 280.0F) {
            mOffsetX = paramMotionEvent.getX();
        } else if (mOffsetX - paramMotionEvent.getX() < 0.0F) {
            pivotX = 0.0F;
        } else {
            pivotX = 280.0F;
        }
        pivotX = pivotX / 280.0F * 1000.0F;
        reloadColorTempSlider(pivotX);
        if (paramMotionEvent.getAction() != MotionEvent.ACTION_UP) {
            if (mListener != null) {
                mListener.onBeganUpdateColorTemp(pivotX);
            }
        } else if (mListener != null) {
            mListener.onEndedUpdateColorTemp(pivotX);
        }
    }

    private void initView() {
        this.thumbImageView = new ImageView(getContext());
        this.thumbImageView.setImageResource(R.mipmap.colortemp_circular_slider_thumb);
        addView(this.thumbImageView);
    }

    @Override
    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        reloadColorTempSlider(this.colorTempValue);
        if (mPoint.x != getWidth() && mPoint.y != getHeight()) {
            mPoint.x = getWidth();
            mPoint.y = getHeight();
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) thumbImageView.getLayoutParams();
            paramInt1 = (int) (getWidth() * 0.13F);
            layoutParams.width = paramInt1;
            layoutParams.height = paramInt1;
            thumbImageView.setLayoutParams(layoutParams);
        }
    }

    @Override
    protected void onMeasure(int paramInt1, int paramInt2) {
        super.onMeasure(paramInt1, paramInt2);
    }

    @Override
    public boolean onTouchEvent(MotionEvent paramMotionEvent) {
        switch (paramMotionEvent.getAction()) {
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                colorTempSliderEvent(paramMotionEvent);
            case MotionEvent.ACTION_DOWN:
            default:
                break;
        }
        return true;
    }

    public void reloadColorTempSlider(float paramFloat) {
        this.colorTempValue = Math.min(1000.0F, Math.max(0.0F, paramFloat));
        if (thumbImageView != null) {
            paramFloat = (float) (0.7853981633974483D - (100.0F - colorTempValue * 100.0F / 1000.0F) * 4.71238898038469D / 100.0D);
//            float f1 = getHeight() * 0.41F;
            PointF pointF = new PointF(0.0F, 0.0F);
            double d1 = Math.abs(getHeight() * 0.41F);
            pointF.x = getPivotX() + (float) (d1 * Math.cos(paramFloat));
            pointF.y = getPivotY() + (float) (d1 * Math.sin(paramFloat));
            thumbImageView.setX(pointF.x - thumbImageView.getPivotX());
            thumbImageView.setY(pointF.y - thumbImageView.getPivotY());
        }
    }

    public void setOnUpdateColorTempListener(OnUpdateColorTempListener paramOnUpdateColorTempListener) {
        this.mListener = paramOnUpdateColorTempListener;
    }

    public static interface OnUpdateColorTempListener {
        void onBeganUpdateColorTemp(float param1Float);

        void onEndedUpdateColorTemp(float param1Float);
    }
}
