package com.onmicro.snc_ble.dialog.PopupMenu;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class PopupMenuItem {
    public Drawable mDrawable;

    public CharSequence mTitle;

    public PopupMenuItem(Context paramContext, int paramInt1, int paramInt2) {
        this.mTitle = paramContext.getResources().getText(paramInt1);
        this.mDrawable = paramContext.getResources().getDrawable(paramInt2);
    }

    public PopupMenuItem(Context paramContext, CharSequence paramCharSequence) {
        this.mTitle = paramCharSequence;
        this.mDrawable = null;
    }

    public PopupMenuItem(Context paramContext, CharSequence paramCharSequence, int paramInt) {
        this.mTitle = paramCharSequence;
        this.mDrawable = paramContext.getResources().getDrawable(paramInt);
    }

    public PopupMenuItem(Drawable paramDrawable, CharSequence paramCharSequence) {
        this.mDrawable = paramDrawable;
        this.mTitle = paramCharSequence;
    }
}
