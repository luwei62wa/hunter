package com.onmicro.snc_ble.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.dialog.OptAnimationLoader;
import com.iflytek.idata.extension.IFlyCollectorExt;
import com.weigan.loopview.LoopView;

import java.util.ArrayList;
import java.util.List;

public class PickerView extends Dialog implements View.OnClickListener {
    private List<String> hours = new ArrayList<>();

    private int mAlertType;

    private Button mCancelButton;

    private OnClickListener mCancelClickListener;

    private String mCancelText;

    private boolean mCloseFromCancel;

    private Button mConfirmButton;

    private OnClickListener mConfirmClickListener;

    private String mConfirmText;

    private String mContentText;

    private View mDialogView;

    private LoopView mLoopViewHour;

    private LoopView mLoopViewMinute;

    private LoopView mLoopViewSecond;

    private AnimationSet mModalInAnim;

    private AnimationSet mModalOutAnim;

    private Animation mOverlayOutAnim;

    private boolean mShowCancel;

    private boolean mShowContent;

    private String mTitleText;

    private TextView mTitleTextView;

    private List<String> minutes = new ArrayList<>();

    private List<String> seconds = new ArrayList<>();

    public PickerView(Context paramContext) {
        this(paramContext, 0);
    }

    public PickerView(Context paramContext, int paramInt) {
        super(paramContext, R.style.sweet_alert_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        mModalInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_in);
        mModalOutAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_out);
        mModalOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation param1Animation) {
                mDialogView.setVisibility(View.GONE);
                mDialogView.post(() -> {
                    if (mCloseFromCancel) {
                        cancel();
                        return;
                    }
                    dismiss();
                });
            }

            @Override
            public void onAnimationRepeat(Animation param1Animation) {
            }

            @Override
            public void onAnimationStart(Animation param1Animation) {
            }
        });

        mOverlayOutAnim = new Animation() {
            @Override
            protected void applyTransformation(float param1Float, Transformation param1Transformation) {
                Window window = getWindow();
                if (window != null) {
                    WindowManager.LayoutParams layoutParams = window.getAttributes();
                    layoutParams.alpha = 1.0F - param1Float;
                    window.setAttributes(layoutParams);
                }
            }
        };
        mOverlayOutAnim.setDuration(120L);
    }

    private void dismissWithAnimation(boolean paramBoolean) {
        mCloseFromCancel = paramBoolean;
        mConfirmButton.startAnimation(mOverlayOutAnim);
        mDialogView.startAnimation(mModalOutAnim);
    }

    @Override
    public void cancel() {
        dismissWithAnimation(true);
    }

    public void dismissWithAnimation() {
        dismissWithAnimation(false);
    }

    public String getCancelText() {
        return mCancelText;
    }

    public String getConfirmText() {
        return mConfirmText;
    }

    public String getTitleText() {
        return mTitleText;
    }

    public boolean isShowCancelButton() {
        return mShowCancel;
    }

    @Override
    public void onClick(View paramView) {
        if (paramView.getId() == R.id.cancel_button) {
            if (mCancelClickListener != null)
                mCancelClickListener.onClick("", "", "", 0L);
            dismissWithAnimation();
            return;
        }
        if (paramView.getId() == R.id.confirm_button) {
            if (this.mConfirmClickListener != null) {
                String str1 = hours.get(mLoopViewHour.getSelectedItem());
                String str2 = minutes.get(mLoopViewMinute.getSelectedItem());
                String str3 = seconds.get(mLoopViewSecond.getSelectedItem());
                long l = ((Integer.parseInt(str1) * 3600 + Integer.parseInt(str2) * 60 + Integer.parseInt(str3)) * 1000);
                mConfirmClickListener.onClick(str1, str2, str3, l);
            }
            dismissWithAnimation();
        }
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.picker_view);
        Window window = getWindow();
        if (window != null) {
            mDialogView = window.getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
        }
        mTitleTextView = findViewById(R.id.title_text);
        mLoopViewHour = findViewById(R.id.loopviewhour);
        mLoopViewMinute = findViewById(R.id.loopviewminute);
        mLoopViewSecond = findViewById(R.id.loopviewsecond);
        mConfirmButton = findViewById(R.id.confirm_button);
        mCancelButton = findViewById(R.id.cancel_button);
        mConfirmButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
//        mLoopViewHour.setDividerColor(getContext().getResources().getColor(R.color.whiteColor));
//        mLoopViewMinute.setDividerColor(getContext().getResources().getColor(R.color.whiteColor));
//        mLoopViewSecond.setDividerColor(getContext().getResources().getColor(R.color.whiteColor));
        byte b;
        for (b = 0; b < 24; b++) {
            List list = this.hours;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(b);
            list.add(stringBuilder.toString());
        }
        for (b = 0; b < 60; b++) {
            List list = this.minutes;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(b);
            list.add(stringBuilder.toString());
        }
        for (b = 0; b < 60; b++) {
            List list = this.seconds;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(b);
            list.add(stringBuilder.toString());
        }
        mLoopViewHour.setItems(hours);
        mLoopViewMinute.setItems(minutes);
        mLoopViewSecond.setItems(seconds);
        mLoopViewHour.setInitPosition(0);
        mLoopViewMinute.setInitPosition(0);
        mLoopViewSecond.setInitPosition(0);
        setTitleText(mTitleText);
        setCancelText(mCancelText);
        setConfirmText(mConfirmText);
    }

    @Override
    protected void onStart() {
        this.mDialogView.startAnimation(this.mModalInAnim);
    }

    public PickerView setCancelClickListener(OnClickListener paramOnClickListener) {
        mCancelClickListener = paramOnClickListener;
        return this;
    }

    public PickerView setCancelText(String paramString) {
        mCancelText = paramString;
        if (mCancelButton != null && mCancelText != null) {
            showCancelButton(true);
            mCancelButton.setText(mCancelText);
        }
        return this;
    }

    public PickerView setConfirmClickListener(OnClickListener paramOnClickListener) {
        mConfirmClickListener = paramOnClickListener;
        return this;
    }

    public PickerView setConfirmText(String paramString) {
        mConfirmText = paramString;
        if (mConfirmButton != null && mConfirmText != null)
            mConfirmButton.setText(mConfirmText);
        return this;
    }

    public PickerView setTitleText(String paramString) {
        mTitleText = paramString;
        if (mTitleTextView != null && mTitleText != null)
            mTitleTextView.setText(mTitleText);
        return this;
    }

    public PickerView showCancelButton(boolean paramBoolean) {
        mShowCancel = paramBoolean;
        if (mCancelButton != null) {
            if (this.mShowCancel) {
                mCancelButton.setVisibility(View.VISIBLE);
            } else {
                mCancelButton.setVisibility(View.GONE);
            }
        }
        return this;
    }

    public static interface OnClickListener {
        void onClick(String param1String1, String param1String2, String param1String3, long param1Long);
    }
}
