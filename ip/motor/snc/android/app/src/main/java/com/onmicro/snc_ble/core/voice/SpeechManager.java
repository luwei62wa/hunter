package com.onmicro.snc_ble.core.voice;

import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.onmicro.snc_ble.app.App;
import com.github.promeg.pinyinhelper.Pinyin;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.idata.extension.IFlyCollectorExt;

public class SpeechManager {
    public static final int SPEECH_STYLE_RECOGNIZER = 0;
    public static final int SPEECH_STYLE_SYNTHESIZER = 1;
    public static final int SPEECH_STYLE_STOP = 2;


    private static SpeechManager instance;

    private SpeechManager() {
    }

    public static SpeechManager getInstance() {
        if (instance == null) {
            synchronized (SpeechManager.class) {
                if (instance == null) {
                    instance = new SpeechManager();
                }
            }
        }
        return instance;
    }

    private SpeechCallback mCallback;

    private String[] mDataStrings = {
            "你可以说\"黄光\"", "你可以说\"白光\"", "你可以说\"全亮\"", "你可以说\"小夜灯\"", "你可以说\"辅助光\"", "你可以说\"开灯\"", "你可以说\"关灯\"", "你可以说\"亮度百分之二十\"", "你可以说\"亮度百分之四十\"", "你可以说\"亮度百分之六十\"",
            "你可以说\"亮度百分之八十\"", "你可以说\"亮度百分之百\""};

    private String[] mFanDataStrings = {
            "你可以说\"黄光\"", "你可以说\"白光\"", "你可以说\"全亮\"", "你可以说\"小夜灯\"", "你可以说\"辅助光\"", "你可以说\"开灯\"", "你可以说\"关灯\"", "你可以说\"低档\"", "你可以说\"中档\"", "你可以说\"高档\"",
            "你可以说\"正转\"", "你可以说\"反转\"", "你可以说\"关闭风扇\"", "你可以说\"亮度百分之二十\"", "你可以说\"亮度百分之四十\"", "你可以说\"亮度百分之六十\"", "你可以说\"亮度百分之八十\"", "你可以说\"亮度百分之百\""
    };

    private int mGroupedStyle = 0;

    private SpeechRecognizer mIat = SpeechRecognizer.createRecognizer(App.getInstance(), this.mInitListener);
    private InitListener mInitListener = code -> {
    };
    private RecognizerListener mRecognizerListener = new RecognizerListener() {
        @Override
        public void onBeginOfSpeech() {
        }

        @Override
        public void onEndOfSpeech() {
            // 此回调表示：检测到了语音的尾端点，已经进入识别过程，不再接受语音输入
            if (SpeechManager.this.style == 2)
                return;
            mCallback.endSpeech();
        }

        @Override
        public void onError(SpeechError error) {
        }

        @Override
        public void onEvent(int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle) {
        }

        @Override
        public void onResult(RecognizerResult results, boolean param1Boolean) {
            printResult(results);
        }

        @Override
        public void onVolumeChanged(int volume, byte[] data) {
            if (mVolume != volume) {
                // 猜测
//                SpeechManager.access$402(SpeechManager.this, param1Int);
                mVolume = volume;
                mCallback.onVolumeChanged(volume);
            }
        }
    };

    private String mResultText = null;

    private SpeechSynthesizer mTts = SpeechSynthesizer.createSynthesizer(App.getInstance(), this.mTtsInitListener);
    private InitListener mTtsInitListener = code -> {

    };

    private SynthesizerListener mTtsListener = new SynthesizerListener() {
        @Override
        public void onBufferProgress(int param1Int1, int param1Int2, int param1Int3, String param1String) {
        }

        @Override
        public void onCompleted(SpeechError param1SpeechError) {
            if (style == 2) {
                return;
            }
            if (param1SpeechError == null) {
                startListening();
            } else {
                Log.e("test", param1SpeechError.toString());
            }
        }

        @Override
        public void onEvent(int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle) {
        }

        @Override
        public void onSpeakBegin() {
        }

        @Override
        public void onSpeakPaused() {
        }

        @Override
        public void onSpeakProgress(int param1Int1, int param1Int2, int param1Int3) {
        }

        @Override
        public void onSpeakResumed() {
        }
    };

    private int mVolume = -1;

    private int style = 2;

    private void endOfSpeech() {

        if (this.mResultText == null) {
            speakingMessage("");
            return;
        }
        String str2 = this.mResultText;
        this.mResultText = toPinyin(this.mResultText);
        String str1 = "";
        int i = mResultText.indexOf(toPinyin("黄光"));
        int j = 3;
        int m = 1;
        int k = -1;
        if (i != -1 || str2.contains("黄")) {
            str1 = "黄光";
            i = -1;
            j = 0;
        } else if (mResultText.contains(toPinyin("白"))) {
            str1 = "白光";
            i = -1;
            j = 1;
        } else if (mResultText.contains(toPinyin("中性")) || mResultText.contains(toPinyin("全亮")) || mResultText.contains(toPinyin("最亮"))) {
            str1 = "全亮";
            i = -1;
            j = 2;
        } else {
            i = -1;
            if (mResultText.contains(toPinyin("小")) || mResultText.contains(toPinyin("夜"))) {
                str1 = "小夜灯";
            } else if (mResultText.contains(toPinyin("辅助")) || str2.contains("辅") || str2.contains("助")) {
                str1 = "辅助光";
                i = -1;
                j = 4;
            } else if (mResultText.contains(toPinyin("开灯")) || str2.contains("开")) {
                str1 = "开灯";
                i = -1;
                j = 5;
            } else if (mResultText.contains(toPinyin("关灯"))) {
                str1 = "关灯";
                j = 6;
            } else {
                if (mResultText.contains(toPinyin("低"))) {
                    str1 = "低档";
                    i = -1;
                    j = -1;
                    k = 0;
                } else if (mResultText.contains(toPinyin("中"))) {
                    str1 = "中档";
                    i = -1;
                    j = -1;
                    k = 1;
                } else if (mResultText.contains(toPinyin("高"))) {
                    str1 = "高档";
                    i = -1;
                    j = -1;
                    k = 2;
                } else if (mResultText.contains(toPinyin("正转")) || str2.contains("正")) {
                    str1 = "正转";
                    i = -1;
                    j = -1;
                    k = 3;
                } else if (mResultText.contains(toPinyin("反转")) || (str2.contains("反"))) {
                    str1 = "反转";
                    i = -1;
                    j = -1;
                    k = 4;
                } else if (mResultText.contains(toPinyin("关闭风扇"))) {
                    str1 = "关闭风扇";
                    i = -1;
                    j = -1;
                    k = 5;
                } else {
                    j = -1;
                    if (mResultText.contains("100") || mResultText.contains(toPinyin("一百"))) {
                        str1 = "亮度百分之百";
                        i = 1000;
                    } else if (mResultText.contains("10") || mResultText.contains(toPinyin("十"))) {
                        str1 = "亮度百分之十";
                        i = 100;
                    } else if (mResultText.contains("20") || mResultText.contains(toPinyin("二十"))) {
                        str1 = "亮度百分之二十";
                        i = 200;
                    } else if (mResultText.contains("30") || mResultText.contains(toPinyin("三十"))) {
                        str1 = "亮度百分之三十";
                        i = 300;
                    } else if (mResultText.contains("40") || mResultText.contains(toPinyin("四十"))) {
                        str1 = "亮度百分之四十";
                        i = 400;
                    } else if (mResultText.contains("50") || mResultText.contains(toPinyin("五十"))) {
                        str1 = "亮度百分之五十";
                        i = 500;
                    } else if (mResultText.contains("60") || mResultText.contains(toPinyin("六十"))) {
                        str1 = "亮度百分之六十";
                        i = 600;
                    } else if (mResultText.contains("70") || mResultText.contains(toPinyin("七十"))) {
                        str1 = "亮度百分之七十";
                        i = 700;
                    } else if (mResultText.contains("80") || mResultText.contains(toPinyin("八十"))) {
                        str1 = "亮度百分之八十";
                        i = 800;
                    } else if (mResultText.contains("90") || mResultText.contains(toPinyin("九十"))) {
                        str1 = "亮度百分之九十";
                        i = 900;
                    } else {
                        i = -1;
                        j = -1;
                        m = 0;
                    }
                }
            }
        }
        int n = m;
        if (mGroupedStyle != 8) {
            n = m;
            if (k >= 0) {
                n = 0;
            }
        }
        if (n != 0) {
            if (j >= 0) {
                mCallback.onCompleted(j);
            }
            if (k >= 0) {
                mCallback.onFan(k);
            }
            if (i >= 0) {
                mCallback.onBrightness(i);
            }
            mCallback.onMessage("已调节", str1);
            startSpeaking("已调节");
            return;
        }

        speakingMessage(this.mResultText);
    }

    private void printResult(RecognizerResult paramRecognizerResult) {
        if (mResultText == null) {
            mResultText = JsonParser.parseIatResult(paramRecognizerResult.getResultString());
            endOfSpeech();
        }
    }

    private void speakingMessage(String paramString) {
        String str;
        if (mGroupedStyle == 8) {
            str = mFanDataStrings[(int) (Math.random() * mFanDataStrings.length)];
        } else {
            str = mDataStrings[(int) (Math.random() * mDataStrings.length)];
        }
        startSpeaking(str);
        mCallback.onMessage(str, paramString);
    }

    private void startListening() {
        mVolume = -1;
        style = 0;
        mResultText = null;
        setListeningParam();
        mIat.startListening(mRecognizerListener);
    }

    private void startSpeaking(String paramString) {
        style = 1;
        setSpeakingParam();
        mTts.startSpeaking(paramString, mTtsListener);
    }

    private String toPinyin(String paramString) {
        String[] arrayOfString = Pinyin.toPinyin(paramString, "&").split("&");
        int i = arrayOfString.length;
        StringBuilder paramStringBuilder = new StringBuilder();
        for (byte b = 0; b < i; b++) {
            String str = arrayOfString[b];
            str.indexOf("G");
            if (str.endsWith("G")) {
                paramStringBuilder.append(str.substring(0, str.length() - 1));
            } else {
                paramStringBuilder.append(str);
            }
        }
        paramString = paramStringBuilder.toString();
        return paramString;
    }

    public void pauseSpeech() {
        if (style == 0) {
            mIat.stopListening();
        }
        if (style == 1) {
            mTts.pauseSpeaking();
        }
    }

    public void resumeSpeech() {
        if (style == 0) {
            startListening();
        }
        if (style == 1) {
            mTts.resumeSpeaking();
        }
    }

    public void setListeningParam() {
        mIat.setParameter(SpeechConstant.PARAMS, null);
        mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");
        mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
        mIat.setParameter(SpeechConstant.ACCENT, "mandarin");
        mIat.setParameter(SpeechConstant.VAD_BOS, "5000");
        mIat.setParameter(SpeechConstant.VAD_EOS, "1000");
        mIat.setParameter(SpeechConstant.ASR_PTT, "1");
        mIat.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
        String stringBuilder = Environment.getExternalStorageDirectory() + "/msc/iat.wav";
        mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, stringBuilder);
    }

    private void setSpeakingParam() {
        mTts.setParameter(SpeechConstant.PARAMS, null);
        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        mTts.setParameter(SpeechConstant.TTS_DATA_NOTIFY, "1");
        mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
        mTts.setParameter(SpeechConstant.SPEED, "50");
        mTts.setParameter(SpeechConstant.PITCH, "50");
        mTts.setParameter(SpeechConstant.VOLUME, "50");
        mTts.setParameter(SpeechConstant.STREAM_TYPE, String.valueOf(AudioManager.STREAM_MUSIC));
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");
        mTts.setParameter(SpeechConstant.AUDIO_FORMAT, "pcm");
        String savePath = Environment.getExternalStorageDirectory() + "/msc/tts.pcm";
        mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, savePath);
    }

    public void startSpeaking(String paramString, int paramInt, SpeechCallback
            paramSpeechCallback) {
        mGroupedStyle = paramInt;
        mCallback = paramSpeechCallback;
        startSpeaking(paramString);
    }

    public void stopSpeech() {
        style = 2;
        mIat.stopListening();
        mTts.stopSpeaking();
    }

    public static abstract class SpeechCallback {
        public abstract void onBrightness(int param1Int);

        public abstract void onCompleted(int param1Int);

        public abstract void onFan(int param1Int);

        public abstract void onMessage(String param1String1, String param1String2);

        public abstract void onVolumeChanged(int param1Int);

        public abstract void endSpeech();
    }
}


