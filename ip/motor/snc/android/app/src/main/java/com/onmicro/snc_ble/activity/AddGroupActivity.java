package com.onmicro.snc_ble.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.adpter.SectionAddGroupAdapter;
import com.onmicro.snc_ble.models.AddGroupModel;
import com.onmicro.snc_ble.models.SectionAddGroup;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddGroupActivity extends BaseActivity {
    @BindView(R.id.backgroundview)
    View backgroundView;

    @BindView(R.id.create_text)
    TextView createText;

    private List<SectionAddGroup> dataSource = new ArrayList<>();

    @BindView(R.id.donebutton)
    Button doneButton;

    @BindView(R.id.edittext)
    EditText editText;

    private SectionAddGroupAdapter groupAdapter;

    @BindView(R.id.text_title1)
    TextView inputTitleText;

    @BindView(R.id.inputview)
    View inputView;

    @BindView(R.id.msg_text)
    TextView msgText;

    @BindView(R.id.msg_text1)
    TextView msgText1;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private int selectedIndex = 1;

    @BindView(R.id.text_title)
    TextView selectedTitleText;

    private void updateText() {
        selectedTitleText.setText(R.string.selected_title);
        inputTitleText.setText(R.string.input_title);
        createText.setText(R.string.create);
        msgText.setText(R.string.tip_msg_explain);
        msgText1.setText(R.string.tip_msg_explain1);
        doneButton.setText(R.string.done);
    }

    public void addGroupButtonClicked(View paramView) {
        String str1;
        SectionAddGroup sectionAddGroup = dataSource.get(this.selectedIndex);
        if (paramView.getId() != R.id.addbutton) {
            str1 = this.editText.getText().toString();
        } else {
            str1 = sectionAddGroup.t.getGroupName();
        }
        Intent intent = new Intent();
        String str2 = str1;
        if (str1 == null)
            str2 = "";
        intent.putExtra("groupName", str2);
        intent.putExtra("style", sectionAddGroup.t.getStyle());
        setResult(RESULT_OK, intent);
        back(null);
    }

    public void addRecyclerView() {
        this.groupAdapter = new SectionAddGroupAdapter(R.layout.row_add_group, R.layout.row_add_group_header, this.dataSource);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gridLayoutManager.setOrientation(1);
        this.recyclerView.setLayoutManager(gridLayoutManager);
        this.recyclerView.setAdapter(this.groupAdapter);
        this.groupAdapter.setOnItemClickListener((param1BaseQuickAdapter, param1View, param1Int) -> {
            SectionAddGroup sectionAddGroup = dataSource.get(param1Int);
            if (!sectionAddGroup.isHeader) {
                if (sectionAddGroup.t == null)
                    return;
                dataSource.get(selectedIndex).t.setSelected(false);
                dataSource.get(param1Int).t.setSelected(true);
//                    ((AddGroupModel)((SectionAddGroup)this.this$0.dataSource.get(this.this$0.selectedIndex)).t).setSelected(Boolean.valueOf(false));
//                    ((AddGroupModel)((SectionAddGroup)this.this$0.dataSource.get(param1Int)).t).setSelected(Boolean.valueOf(true));
                // 猜测 AddGroupActivity.access$102(AddGroupActivity.this, param1Int);
                selectedIndex = param1Int;
                AddGroupActivity.this.groupAdapter.setNewData(AddGroupActivity.this.dataSource);
                if (param1Int == 8 || param1Int == 11) {
                    AddGroupActivity.this.backgroundView.setVisibility(View.GONE);
                    AddGroupActivity.this.inputView.setVisibility(View.VISIBLE);
                    AddGroupActivity.this.inputView.setFocusable(true);
                    AddGroupActivity.this.inputView.setFocusableInTouchMode(true);
                    AddGroupActivity.this.inputView.requestFocus();
                    AddGroupActivity.this.getWindow().setSoftInputMode(5);
                }
            }
        });
    }

    public void loadDataSource() {
        this.dataSource.add(new SectionAddGroup(true, getString(R.string.grouped_header_style000)));
        AddGroupModel addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style000));
        addGroupModel1.setStyle(0);
        addGroupModel1.setSelected(true);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style001));
        addGroupModel1.setStyle(1);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style002));
        addGroupModel1.setStyle(2);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        AddGroupModel addGroupModel2 = new AddGroupModel();
        addGroupModel2.setGroupName(getString(R.string.grouped_style003));
        addGroupModel1.setStyle(3);
        addGroupModel2.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel2));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style004));
        addGroupModel1.setStyle(4);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style005));
        addGroupModel1.setStyle(5);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style006));
        addGroupModel1.setStyle(6);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style007));
        addGroupModel1.setStyle(7);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        this.dataSource.add(new SectionAddGroup(true, getString(R.string.grouped_header_style001)));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style008));
        addGroupModel1.setStyle(8);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
        addGroupModel1 = new AddGroupModel();
        addGroupModel1.setGroupName(getString(R.string.grouped_style009));
        addGroupModel1.setStyle(8);
        addGroupModel1.setSelected(false);
        this.dataSource.add(new SectionAddGroup(addGroupModel1));
    }

    @Override
    public void onClickedBackBarButtonAction(View paramView) {
        Intent intent = new Intent();
        setResult(-1, intent);
        intent.putExtra("groupName", "");
        back(null);
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_add_group);
        ButterKnife.bind(this);
        addRecyclerView();
        loadDataSource();
        StatusBarUtil.setTransparent(this);
        updateText();
    }
}
