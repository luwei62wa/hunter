package com.onmicro.snc_ble.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.onmicro.snc_ble.R;


public class InputDialog extends Dialog implements View.OnClickListener {
    public static final int CUSTOM_IMAGE_TYPE = 4;

    public static final int ERROR_TYPE = 1;

    public static final int NORMAL_TYPE = 0;

    public static final int PROGRESS_TYPE = 5;

    public static final int SUCCESS_TYPE = 2;

    public static final int WARNING_TYPE = 3;

    private int gravity = 80;

    private int mAlertType;

    private Button mCancelButton;

    private OnClickListener mCancelClickListener;

    private String mCancelText;

    private boolean mCloseFromCancel;

    private Button mConfirmButton;

    private OnClickListener mConfirmClickListener;

    private String mConfirmText;

    private View mDialogView;

    private AnimationSet mModalInAnim;

    private AnimationSet mModalOutAnim;

    private Animation mOverlayOutAnim;

    private boolean mShowCancel;

    private String mTitleText;

    private TextView mTitleTextView;

    private EditText mUserNameEditText;

    public InputDialog(Context paramContext) {
        this(paramContext, 0);
    }

    public InputDialog(Context paramContext, int paramInt) {
        super(paramContext, R.style.sweet_alert_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        this.mAlertType = paramInt;
        this.mModalInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_in);
        this.mModalOutAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_out);
        this.mModalOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation param1Animation) {
                InputDialog.this.mDialogView.setVisibility(View.GONE);
                InputDialog.this.mDialogView.post(() -> {
                    if (mCloseFromCancel) {
                        cancel();
                        return;
                    }
                    dismiss();
                });
            }

            @Override
            public void onAnimationRepeat(Animation param1Animation) {
            }

            @Override
            public void onAnimationStart(Animation param1Animation) {
            }
        });
        this.mOverlayOutAnim = new Animation() {
            protected void applyTransformation(float param1Float, Transformation param1Transformation) {
                WindowManager.LayoutParams layoutParams = InputDialog.this.getWindow().getAttributes();
                layoutParams.alpha = 1.0F - param1Float;
                InputDialog.this.getWindow().setAttributes(layoutParams);
            }
        };
        this.mOverlayOutAnim.setDuration(120L);
    }

    private void changeAlertType(int paramInt, boolean paramBoolean) {
        this.mAlertType = paramInt;
        if (this.mDialogView != null) {
            if (!paramBoolean)
                restore();
            switch (this.mAlertType) {

            }
        }
    }

    private void dismissWithAnimation(boolean paramBoolean) {
        this.mCloseFromCancel = paramBoolean;
        this.mConfirmButton.startAnimation(this.mOverlayOutAnim);
        this.mDialogView.startAnimation(this.mModalOutAnim);
    }

    private void restore() {
        this.mConfirmButton.setVisibility(View.VISIBLE);
        this.mConfirmButton.setBackgroundResource(R.drawable.dialog_alert_left_btn_bg);
    }

    @Override
    public void cancel() {
        dismissWithAnimation(true);
    }

    public void changeAlertType(int paramInt) {
        changeAlertType(paramInt, false);
    }

    public void dismissWithAnimation() {
        dismissWithAnimation(false);
    }

    public int getAlerType() {
        return this.mAlertType;
    }

    public String getCancelText() {
        return this.mCancelText;
    }

    public String getConfirmText() {
        return this.mConfirmText;
    }

    public String getTitleText() {
        return this.mTitleText;
    }

    public boolean isShowCancelButton() {
        return this.mShowCancel;
    }

    @Override
    public void onClick(View paramView) {
        if (paramView.getId() == R.id.cancel_button) {
            if (this.mCancelClickListener != null)
                this.mCancelClickListener.onClick(this, this.mUserNameEditText.getText().toString());
            dismissWithAnimation();
            return;
        }
        if (paramView.getId() == R.id.confirm_button) {
            if (this.mConfirmClickListener != null)
                this.mConfirmClickListener.onClick(this, this.mUserNameEditText.getText().toString());
            dismissWithAnimation();
        }
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.dialog_input);
        Window window = getWindow();
        if (window != null) {
            window.setGravity(80);
            mDialogView = window.getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
        }

        mTitleTextView = findViewById(R.id.title_text);
        mUserNameEditText = findViewById(R.id.user_name);
        mConfirmButton = findViewById(R.id.confirm_button);
        mCancelButton = findViewById(R.id.cancel_button);
        mConfirmButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
        mUserNameEditText.setFocusable(true);
        mUserNameEditText.setFocusableInTouchMode(true);
        mUserNameEditText.requestFocus();
        if (window != null) {
            window.setSoftInputMode(5);
        }
        setTitleText(this.mTitleText);
        setCancelText(this.mCancelText);
        setConfirmText(this.mConfirmText);
        changeAlertType(this.mAlertType, true);
        mUserNameEditText.setOnEditorActionListener((param1TextView, param1Int, param1KeyEvent) -> {
            if (gravity != 17) {
                if (window != null) {
                    window.setGravity(17);
                }
                // 猜测 InputDialog.access$402(InputDialog.this, 17);
//                changeAlertType(17);
                gravity = 17;
            }
            return false;
        });
        mUserNameEditText.setOnTouchListener((param1View, param1MotionEvent) -> {
            if (gravity != 80) {
                if (window != null){
                    window.setGravity(80);
                }
                // 猜测 InputDialog.access$402(InputDialog.this, 80);
//                changeAlertType(80);
                gravity = 80;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        this.mDialogView.startAnimation(this.mModalInAnim);
    }

    public InputDialog setCancelClickListener(OnClickListener paramOnClickListener) {
        this.mCancelClickListener = paramOnClickListener;
        return this;
    }

    public InputDialog setCancelText(String paramString) {
        this.mCancelText = paramString;
        if (this.mCancelButton != null && this.mCancelText != null) {
            showCancelButton(true);
            this.mCancelButton.setText(this.mCancelText);
        }
        return this;
    }

    public InputDialog setConfirmClickListener(OnClickListener paramOnClickListener) {
        this.mConfirmClickListener = paramOnClickListener;
        return this;
    }

    public InputDialog setConfirmText(String paramString) {
        this.mConfirmText = paramString;
        if (this.mConfirmButton != null && this.mConfirmText != null)
            this.mConfirmButton.setText(this.mConfirmText);
        return this;
    }

    public InputDialog setTitleText(String paramString) {
        this.mTitleText = paramString;
        if (this.mTitleTextView != null && this.mTitleText != null)
            this.mTitleTextView.setText(this.mTitleText);
        return this;
    }

    public InputDialog showCancelButton(boolean paramBoolean) {
        mShowCancel = paramBoolean;
        if (mCancelButton != null) {
            if (paramBoolean){
                mCancelButton.setVisibility(View.VISIBLE);
            }else {
                mCancelButton.setVisibility(View.GONE);
            }
        }
        return this;
    }

    public static interface OnClickListener {
        void onClick(InputDialog param1InputDialog, String param1String);
    }
}
