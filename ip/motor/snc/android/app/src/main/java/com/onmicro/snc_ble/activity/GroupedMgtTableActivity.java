package com.onmicro.snc_ble.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.adpter.GroupedMgtAdapter;
import com.onmicro.snc_ble.core.db.DBGroup;
import com.onmicro.snc_ble.dialog.InputDialog;
import com.onmicro.snc_ble.dialog.SweetAlertDialog;
import com.onmicro.snc_ble.models.GroupModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupedMgtTableActivity extends BaseActivity {
    @BindView(R.id.backBarButton)
    ImageButton backBarButton;

    private List<GroupModel> dataSource = new ArrayList<>();

    private GroupedMgtAdapter groupedAdapter;

    @BindView(R.id.leftBarButton)
    Button leftBarButton;

    private boolean mChange = false;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.rightBarButton)
    ImageButton rightBarButton;

    public void addRecyclerView() {
        groupedAdapter = new GroupedMgtAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(groupedAdapter);
        View view1 = getLayoutInflater().inflate(R.layout.header_view, (ViewGroup) recyclerView.getParent(), false);
        View view2 = getLayoutInflater().inflate(R.layout.header_view, (ViewGroup) recyclerView.getParent(), false);
        groupedAdapter.addHeaderView(view1);
        groupedAdapter.addFooterView(view2);
        groupedAdapter.setOnItemClickListener((param1BaseQuickAdapter, param1View, param1Int) -> {
            GroupModel groupModel = dataSource.get(param1Int);
            groupModel.setSpread(groupModel.getSpread() ^ true);
            groupedAdapter.setNewData(dataSource);
        });
        groupedAdapter.setOnItemChildClickListener((param1BaseQuickAdapter, param1View, position) -> {
            if (param1View.getId() != R.id.changeButton) {
                new SweetAlertDialog(GroupedMgtTableActivity.this).setTitleText(getString(R.string.tip)).setContentText(getString(R.string.tip_msg_deleted)).setCancelText(getString(R.string.deleted)).setConfirmText(getString(R.string.cancel)).setCancelClickListener(param2SweetAlertDialog -> {
//                         猜测    GroupedMgtTableActivity.access$202(GroupedMgtTableActivity.null.this.this$0, true);
                    mChange = true;
                    DBGroup.deleteGroup(dataSource.get(position));
//                            DBGroup.deleteGroup((GroupModel)GroupedMgtTableActivity.null.this.this$0.dataSource.get(position));
                    dataSource.remove(position);
//                            GroupedMgtTableActivity.null.this.this$0.dataSource.remove(position);
                    groupedAdapter.setNewData(dataSource);
//                            GroupedMgtTableActivity.null.this.this$0.groupedAdapter.setNewData(GroupedMgtTableActivity.null.this.this$0.dataSource);
                }).show();
                return;
            }
            new InputDialog(GroupedMgtTableActivity.this).setTitleText(getString(R.string.tip)).setCancelText(getString(R.string.cancel)).setConfirmText(getString(R.string.confirm)).setConfirmClickListener((param2InputDialog, param2String) -> {
//                        GroupedMgtTableActivity.access$202(GroupedMgtTableActivity.null.this.this$0, true);
//                        GroupModel groupModel = (GroupModel)GroupedMgtTableActivity.null.this.this$0.dataSource.get(position);
//                        groupModel.setGroupName(param2String);
//                        GroupedMgtTableActivity.null.this.this$0.groupedAdapter.setNewData(GroupedMgtTableActivity.null.this.this$0.dataSource);
                mChange = true;
                GroupModel groupModel = dataSource.get(position);
                groupModel.setGroupName(param2String);
                groupedAdapter.setNewData(dataSource);
                DBGroup.updateGroup(groupModel);
            }).show();
        });
    }

    public void loadDataSource() {
        dataSource = DBGroup.selectAllGroups();
        dataSource.remove(0);
        for (GroupModel groupModel : dataSource) {
            groupModel.setSpread(false);
        }
        groupedAdapter.replaceData(dataSource);
    }


    @Override
    public void onClickedBackBarButtonAction(View paramView) {
        Intent intent = new Intent();
        intent.putExtra("change", mChange);
        setResult(RESULT_OK, intent);
        back(null);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("change", mChange);
        setResult(RESULT_OK, intent);
        back(null);
        super.onBackPressed();
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
//            Log.e("test2","返回键？？？？");
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_grouped_mgt);
        ButterKnife.bind(this);
        setTitle(getString(R.string.pop_group_edit));
        this.leftBarButton.setVisibility(View.GONE);
        this.rightBarButton.setVisibility(View.GONE);
        addRecyclerView();
        loadDataSource();
    }
}
