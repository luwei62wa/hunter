package com.onmicro.snc_ble.dialog.PopupMenu;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.onmicro.snc_ble.R;

import java.util.ArrayList;

public class PopupMenu extends PopupWindow {
    protected final int LIST_PADDING = 10;

    private Context mContext;

    private boolean mIsDirty;

    private OnItemOnClickListener mItemOnClickListener;

    private ListView mListView;

    private final int[] mLocation = new int[2];

    private ArrayList<PopupMenuItem> mPopupMenuItems = new ArrayList<>();

    private Rect mRect = new Rect();

    private int mScreenHeight;

    private int mScreenWidth;

    private int popupGravity = 0;

    public PopupMenu(Context paramContext) {
        this(paramContext, -2, -2);
    }

    public PopupMenu(Context paramContext, int paramInt1, int paramInt2) {
        mContext = paramContext;
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
        WindowManager windowManager = (WindowManager) paramContext.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            mScreenWidth = windowManager.getDefaultDisplay().getWidth();
            mScreenHeight = windowManager.getDefaultDisplay().getHeight();
        }
        setWidth(paramInt1);
        setHeight(paramInt2);
        setBackgroundDrawable(new BitmapDrawable());
        setContentView(LayoutInflater.from(mContext).inflate(R.layout.popup_menu, null));
        setAnimationStyle(R.style.AnimHead);
        initUi();
    }

    private void initUi() {
        mListView = getContentView().findViewById(R.id.title_list);
        mListView.setOnItemClickListener((param1AdapterView, param1View, param1Int, param1Long) -> {
            dismiss();
            if (mItemOnClickListener != null)
                mItemOnClickListener.onItemClick(mPopupMenuItems.get(param1Int), param1Int);
        });
    }

    private void populateActions() {
        mIsDirty = false;
        mListView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mPopupMenuItems.size();
            }

            @Override
            public Object getItem(int param1Int) {
                return mPopupMenuItems.get(param1Int);
            }

            @Override
            public long getItemId(int param1Int) {
                return param1Int;
            }

            @Override
            public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
                ViewHolder holder;
                if (param1View == null) {
                    param1View = LayoutInflater.from(mContext).inflate(R.layout.popup_menu_item_row, param1ViewGroup, false);
                    holder = new ViewHolder();
                    holder.textView = param1View.findViewById(R.id.txt_title);
                    holder.imageView = param1View.findViewById(R.id.image_view);
                    param1View.setTag(holder);
                } else {
                    holder = (ViewHolder) param1View.getTag();
                }

                holder.textView.setSingleLine(true);
                PopupMenuItem popupMenuItem = mPopupMenuItems.get(param1Int);
                if (popupMenuItem.mTitle != null)
                    holder.textView.setText(popupMenuItem.mTitle);
                if (popupMenuItem.mDrawable != null)
                    holder.imageView.setImageDrawable(popupMenuItem.mDrawable);
                return param1View;
            }
        });
    }

    private class ViewHolder {
        TextView textView;
        ImageView imageView;
    }

    public void addMenuItem(PopupMenuItem paramPopupMenuItem) {
        if (paramPopupMenuItem != null) {
            mPopupMenuItems.add(paramPopupMenuItem);
            mIsDirty = true;
        }
    }

    public void cleanMenuItem() {
        if (mPopupMenuItems.isEmpty()) {
            mPopupMenuItems.clear();
            this.mIsDirty = true;
        }
    }

    public PopupMenuItem getMenuItem(int paramInt) {
        return (paramInt < 0 || paramInt > mPopupMenuItems.size()) ? null : mPopupMenuItems.get(paramInt);
    }

    public void setItemOnClickListener(OnItemOnClickListener paramOnItemOnClickListener) {
        mItemOnClickListener = paramOnItemOnClickListener;
    }

    public void show(View paramView) {
        paramView.getLocationOnScreen(mLocation);
        mRect.set(this.mLocation[0], mLocation[1], mLocation[0] + paramView.getWidth(), mLocation[1] + paramView.getHeight());
        if (mIsDirty)
            populateActions();
        showAtLocation(paramView, popupGravity, mScreenWidth - 10 - getWidth() / 2, mRect.bottom);
    }

    public static interface OnItemOnClickListener {
        void onItemClick(PopupMenuItem param1PopupMenuItem, int param1Int);
    }
}
