package com.onmicro.snc_ble.core.db.dao;


import com.onmicro.snc_ble.models.GroupModel;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import java.util.Map;

public class DaoSession extends AbstractDaoSession {
    private final GroupModelDao groupModelDao;

    private final DaoConfig groupModelDaoConfig;

    public DaoSession(Database paramDatabase, IdentityScopeType paramIdentityScopeType, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig> paramMap) {
        super(paramDatabase);
        this.groupModelDaoConfig = paramMap.get(GroupModelDao.class).clone();
        this.groupModelDaoConfig.initIdentityScope(paramIdentityScopeType);
        this.groupModelDao = new GroupModelDao(this.groupModelDaoConfig, this);
        registerDao(GroupModel.class, this.groupModelDao);
    }

    public void clear() {
        this.groupModelDaoConfig.clearIdentityScope();
    }

    public GroupModelDao getGroupModelDao() {
        return this.groupModelDao;
    }
}
