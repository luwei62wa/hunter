package com.onmicro.snc_ble.macro;


import com.onmicro.snc_ble.app.App;

public class Macro {
    public static final int SCREEN_HEIGHT;

    public static final int SCREEN_WIDTH = (App.getMetrics()).widthPixels;

    static  {
        SCREEN_HEIGHT = (App.getMetrics()).heightPixels;
    }
}
