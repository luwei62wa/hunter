package com.onmicro.snc_ble.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

@SuppressLint({"AppCompatCustomView"})
public class FanImageView extends ImageView {
    private long mLastAnimationValue;

    private ObjectAnimator mRotateAnimator;

    public FanImageView(Context paramContext) {
        this(paramContext, null);
    }

    public FanImageView(Context paramContext, AttributeSet paramAttributeSet) {
        this(paramContext, paramAttributeSet, 0);
    }

    public FanImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        init();
    }

    @TargetApi(21)
    public FanImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
        super(paramContext, paramAttributeSet, paramInt1, paramInt2);
        init();
    }

    private void init() {
        this.mRotateAnimator = ObjectAnimator.ofFloat(this, "rotation", new float[]{0.0F, 360.0F});
        this.mRotateAnimator.setDuration(10L);
        this.mRotateAnimator.setInterpolator(new LinearInterpolator());
        this.mRotateAnimator.setRepeatMode(ValueAnimator.RESTART);
        this.mRotateAnimator.setRepeatCount(-1);
    }

    public void cancelRotateAnimation() {
        this.mLastAnimationValue = 0L;
        this.mRotateAnimator.cancel();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mRotateAnimator != null) {
            this.mRotateAnimator.cancel();
            this.mRotateAnimator = null;
        }
    }

    public void pauseRotateAnimation() {
        this.mLastAnimationValue = this.mRotateAnimator.getCurrentPlayTime();
        this.mRotateAnimator.cancel();
    }

    public void resumeRotateAnimation() {
        this.mRotateAnimator.start();
        this.mRotateAnimator.setCurrentPlayTime(this.mLastAnimationValue);
    }

    public void startRotateAnimation(long paramLong) {
        this.mRotateAnimator.setDuration(paramLong);
        this.mRotateAnimator.cancel();
        this.mRotateAnimator.start();
    }
}
