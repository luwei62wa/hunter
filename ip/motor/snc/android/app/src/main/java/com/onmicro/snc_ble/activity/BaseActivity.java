package com.onmicro.snc_ble.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.app.App;
import com.onmicro.snc_ble.common.LocalizedUtils;
import com.iflytek.cloud.SpeechUtility;

public class BaseActivity extends Activity {
    public static final String PREFER_NAME = "com.iflytek.setting";

    private static final String TAG = MainActivity.class.getSimpleName();

    private final int URL_REQUEST_CODE = 1;

    protected Activity context;

    private void mscInit(String paramString) {
        StringBuilder stringBuffer = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("appid=");
        stringBuilder.append(getString(R.string.app_id));
        stringBuffer.append(stringBuilder.toString());
        stringBuffer.append(",");
        if (!TextUtils.isEmpty(paramString)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("server_url=");
            stringBuilder.append(paramString);
            stringBuffer.append(stringBuilder.toString());
            stringBuffer.append(",");
        }
        SpeechUtility.createUtility(getApplicationContext(), stringBuffer.toString());
    }

    private void mscUninit() {
        if (SpeechUtility.getUtility() != null) {
            SpeechUtility.getUtility().destroy();
            try {
                Thread.sleep(40L);
            } catch (InterruptedException interruptedException) {
                String str = TAG;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("msc uninit failed");
                stringBuilder.append(interruptedException.toString());
                Log.w(str, stringBuilder.toString());
            }
        }
    }

    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(LocalizedUtils.attachBaseContext(paramContext));
    }

    public void back(View paramView) {
        finish();
    }

    public boolean isRunningForeground() {
        ActivityManager systemService = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (systemService != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : systemService.getRunningAppProcesses()) {
                if (runningAppProcessInfo.importance == 100 && runningAppProcessInfo.processName.equals((getApplicationInfo()).processName))
                    return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        if (URL_REQUEST_CODE == paramInt1) {
            Log.d(TAG, "onActivityResult>>");
            try {
                SharedPreferences sharedPreferences = getSharedPreferences("com.iflytek.setting", MODE_PRIVATE);
                String str1 = sharedPreferences.getString("url_preference", "");
                String str2 = sharedPreferences.getString("url_edit", "");
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append("onActivityResult>>domain = ");
                stringBuilder2.append(str2);
                Log.d(TAG, stringBuilder2.toString());
                if (!TextUtils.isEmpty(str2)) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("http://");
                    stringBuilder.append(str2);
                    stringBuilder.append("/msp.do");
                    str1 = stringBuilder.toString();
                }
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("onActivityResult>>server_url = ");
                stringBuilder1.append(str1);
                Log.d(TAG, stringBuilder1.toString());
                mscUninit();
                Thread.sleep(40L);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onClickedBackBarButtonAction(View paramView) {
        back(null);
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        this.context = this;
        App.setContext(this);
    }

    @Override
    public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt) {
        super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    }

    public void requestPermissions() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int i = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (i != 0)
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ,Manifest.permission.LOCATION_HARDWARE
                            , Manifest.permission.READ_PHONE_STATE
                            , Manifest.permission.WRITE_SETTINGS
                            , Manifest.permission.READ_EXTERNAL_STORAGE
                            , Manifest.permission.RECORD_AUDIO
                            ,  Manifest.permission.READ_CONTACTS
                            ,Manifest.permission.ACCESS_FINE_LOCATION
                            ,Manifest.permission.ACCESS_COARSE_LOCATION}, 16);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void setBarBackgroundColor(int paramInt) {
        findViewById(R.id.action_bar).setBackgroundColor(paramInt);
    }

    public void setTitle(String paramString) {
        ((TextView) findViewById(R.id.text_title)).setText(paramString);
    }
}
