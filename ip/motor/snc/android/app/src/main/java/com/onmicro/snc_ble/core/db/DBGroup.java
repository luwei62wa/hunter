package com.onmicro.snc_ble.core.db;

import android.util.Log;

import com.onmicro.snc_ble.core.db.dao.GroupModelDao;
import com.onmicro.snc_ble.models.GroupModel;

import java.util.List;

public class DBGroup {
    public static boolean deleteGroup(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().delete(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void deleteAll(){
        DaoManager.getInstance().getDaoSession().getGroupModelDao().deleteAll();
    }

    public static boolean insertGroupModel(GroupModel paramGroupModel) {
        try {
            long l = DaoManager.getInstance().getDaoSession().insert(paramGroupModel);
            if (l != -1L)
                return true;
        } catch (Exception e) {
            Log.e("ContentValues", e.toString());
        }
        return false;
    }

    public static List<GroupModel> selectAllGroups() {
        return DaoManager.getInstance().getDaoSession().loadAll(GroupModel.class);
    }

    public static boolean selectExistGroupsId(String paramString) {
        try {
            int i = DaoManager.getInstance().getDaoSession().getGroupModelDao().queryBuilder().where(GroupModelDao.Properties.GroupId.eq(paramString), new org.greenrobot.greendao.query.WhereCondition[0]).list().size();
            if (i > 0)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean selectExistGroupsStyle(int paramInt) {
        try {
            paramInt = DaoManager.getInstance().getDaoSession().getGroupModelDao().queryBuilder().where(GroupModelDao.Properties.Style.eq(Integer.valueOf(paramInt)), new org.greenrobot.greendao.query.WhereCondition[0]).list().size();
            if (paramInt > 0)
                return true;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    public static boolean updateAngle(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateBrightness(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateColortemp(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateFeatures(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateGroup(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateSelectedGroup(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateSwitch(GroupModel paramGroupModel) {
        try {
            DaoManager.getInstance().getDaoSession().update(paramGroupModel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
