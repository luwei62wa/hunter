package com.onmicro.snc_ble.core.voice;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonParser {
    public static String parseGrammarResult(String paramString) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            JSONArray jSONArray = (new JSONObject(new JSONTokener(paramString))).getJSONArray("ws");
            for (byte b = 0; b < jSONArray.length(); b++) {
                JSONArray jSONArray1 = jSONArray.getJSONObject(b).getJSONArray("cw");
                for (byte b1 = 0; b1 < jSONArray1.length(); b1++) {
                    JSONObject jSONObject = jSONArray1.getJSONObject(b1);
                    if (jSONObject.getString("w").contains("nomatch")) {
                        stringBuffer.append("没有匹配结果.");
                        return stringBuffer.toString();
                    }
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("【结果】");
                    stringBuilder.append(jSONObject.getString("w"));
                    stringBuffer.append(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("【置信度】");
                    stringBuilder.append(jSONObject.getInt("sc"));
                    stringBuffer.append(stringBuilder.toString());
                    stringBuffer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            stringBuffer.append("没有匹配结果.");
        }
        return stringBuffer.toString();
    }

    public static String parseIatResult(String paramString) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            JSONArray jSONArray = (new JSONObject(new JSONTokener(paramString))).getJSONArray("ws");
            for (byte b = 0; b < jSONArray.length(); b++)
                stringBuffer.append(jSONArray.getJSONObject(b).getJSONArray("cw").getJSONObject(0).getString("w"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    public static String parseLocalGrammarResult(String paramString) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            JSONObject jSONObject = new JSONObject(new JSONTokener(paramString));
            JSONArray jSONArray = jSONObject.getJSONArray("ws");
            for (byte b = 0; ; b++) {
                if (b < jSONArray.length()) {
                    JSONArray jSONArray1 = jSONArray.getJSONObject(b).getJSONArray("cw");
                    for (byte b1 = 0; b1 < jSONArray1.length(); b1++) {
                        JSONObject jSONObject1 = jSONArray1.getJSONObject(b1);
                        if (jSONObject1.getString("w").contains("nomatch")) {
                            stringBuffer.append("没有匹配结果.");
                            return stringBuffer.toString();
                        }
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("【结果】");
                        stringBuilder.append(jSONObject1.getString("w"));
                        stringBuffer.append(stringBuilder.toString());
                        stringBuffer.append("\n");
                    }
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("【置信度】");
                    stringBuilder.append(jSONObject.optInt("sc"));
                    stringBuffer.append(stringBuilder.toString());
                    return stringBuffer.toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            stringBuffer.append("没有匹配结果.");
        }
        return stringBuffer.toString();
    }

    public static String parseTransResult(String paramString1, String paramString2) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            JSONObject jSONObject = new JSONObject(new JSONTokener(paramString1));
            if (!jSONObject.optString("ret").equals("0"))
                return jSONObject.optString("errmsg");
            stringBuffer.append(jSONObject.optJSONObject("trans_result").optString(paramString2));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }
}
