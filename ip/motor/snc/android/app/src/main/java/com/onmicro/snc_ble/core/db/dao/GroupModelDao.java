package com.onmicro.snc_ble.core.db.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.onmicro.snc_ble.models.GroupModel;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class GroupModelDao extends AbstractDao<GroupModel, Long> {
    public static final String TABLENAME = "GROUP_MODEL";

    public GroupModelDao(DaoConfig paramDaoConfig) {
        super(paramDaoConfig);
    }

    public GroupModelDao(DaoConfig paramDaoConfig, DaoSession paramDaoSession) {
        super(paramDaoConfig, paramDaoSession);
    }

    public static void createTable(Database paramDatabase, boolean paramBoolean) {
        String str;
        if (paramBoolean) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CREATE TABLE ");
        stringBuilder.append(str);
        stringBuilder.append("\"GROUP_MODEL\" (\"_id\" INTEGER PRIMARY KEY ,\"STYLE\" INTEGER NOT NULL ,\"GROUP_ID\" TEXT,\"GROUP_NAME\" TEXT,\"COLOR_TEMP\" REAL NOT NULL ,\"BRIGHTNESS\" REAL NOT NULL ,\"TIME_INTERVAL\" INTEGER,\"FEATURES_INDEX\" INTEGER NOT NULL ,\"SPEED\" REAL NOT NULL ,\"IS_SELECTED\" INTEGER,\"IS_ON\" INTEGER,\"SPREAD\" INTEGER);");
        paramDatabase.execSQL(stringBuilder.toString());
    }

    public static void dropTable(Database paramDatabase, boolean paramBoolean) {
        String str;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DROP TABLE ");
        if (paramBoolean) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        stringBuilder.append(str);
        stringBuilder.append("\"GROUP_MODEL\"");
        paramDatabase.execSQL(stringBuilder.toString());
    }

    @Override
    protected final void bindValues(SQLiteStatement paramSQLiteStatement, GroupModel paramGroupModel) {
        paramSQLiteStatement.clearBindings();
        Long long2 = paramGroupModel.getId();
        if (long2 != null)
            paramSQLiteStatement.bindLong(1, long2.longValue());
        paramSQLiteStatement.bindLong(2, paramGroupModel.getStyle());
        String str = paramGroupModel.getGroupId();
        if (str != null)
            paramSQLiteStatement.bindString(3, str);
        str = paramGroupModel.getGroupName();
        if (str != null)
            paramSQLiteStatement.bindString(4, str);
        paramSQLiteStatement.bindDouble(5, paramGroupModel.getColorTemp());
        paramSQLiteStatement.bindDouble(6, paramGroupModel.getBrightness());
        Long long1 = paramGroupModel.getTimeInterval();
        if (long1 != null)
            paramSQLiteStatement.bindLong(7, long1.longValue());
        paramSQLiteStatement.bindLong(8, paramGroupModel.getFeaturesIndex());
        paramSQLiteStatement.bindDouble(9, paramGroupModel.getSpeed());
        Boolean bool2 = paramGroupModel.getIsSelected();
        long l = 0L;
        if (bool2 != null) {
            long l1;
            if (bool2.booleanValue()) {
                l1 = 1L;
            } else {
                l1 = 0L;
            }
            paramSQLiteStatement.bindLong(10, l1);
        }
        bool2 = paramGroupModel.getIsOn();
        if (bool2 != null) {
            long l1;
            if (bool2.booleanValue()) {
                l1 = 1L;
            } else {
                l1 = 0L;
            }
            paramSQLiteStatement.bindLong(11, l1);
        }
        Boolean bool1 = paramGroupModel.getSpread();
        if (bool1 != null) {
            long l1 = l;
            if (bool1.booleanValue())
                l1 = 1L;
            paramSQLiteStatement.bindLong(12, l1);
        }
    }

    @Override
    protected final void bindValues(DatabaseStatement paramDatabaseStatement, GroupModel paramGroupModel) {
        paramDatabaseStatement.clearBindings();
        Long long2 = paramGroupModel.getId();
        if (long2 != null)
            paramDatabaseStatement.bindLong(1, long2.longValue());
        paramDatabaseStatement.bindLong(2, paramGroupModel.getStyle());
        String str = paramGroupModel.getGroupId();
        if (str != null)
            paramDatabaseStatement.bindString(3, str);
        str = paramGroupModel.getGroupName();
        if (str != null)
            paramDatabaseStatement.bindString(4, str);
        paramDatabaseStatement.bindDouble(5, paramGroupModel.getColorTemp());
        paramDatabaseStatement.bindDouble(6, paramGroupModel.getBrightness());
        Long long1 = paramGroupModel.getTimeInterval();
        if (long1 != null)
            paramDatabaseStatement.bindLong(7, long1.longValue());
        paramDatabaseStatement.bindLong(8, paramGroupModel.getFeaturesIndex());
        paramDatabaseStatement.bindDouble(9, paramGroupModel.getSpeed());
        Boolean bool2 = paramGroupModel.getIsSelected();
        long l = 0L;
        if (bool2 != null) {
            long l1;
            if (bool2.booleanValue()) {
                l1 = 1L;
            } else {
                l1 = 0L;
            }
            paramDatabaseStatement.bindLong(10, l1);
        }
        bool2 = paramGroupModel.getIsOn();
        if (bool2 != null) {
            long l1;
            if (bool2.booleanValue()) {
                l1 = 1L;
            } else {
                l1 = 0L;
            }
            paramDatabaseStatement.bindLong(11, l1);
        }
        Boolean bool1 = paramGroupModel.getSpread();
        if (bool1 != null) {
            long l1 = l;
            if (bool1.booleanValue())
                l1 = 1L;
            paramDatabaseStatement.bindLong(12, l1);
        }
    }

    @Override
    public Long getKey(GroupModel paramGroupModel) {
        return (paramGroupModel != null) ? paramGroupModel.getId() : null;
    }

    @Override
    public boolean hasKey(GroupModel paramGroupModel) {
        return (paramGroupModel.getId() != null);
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }

    @Override
    public GroupModel readEntity(Cursor paramCursor, int paramInt) {
        Boolean bool4;
        Boolean bool3;
        Long long2;
        String str2;
        String str1;
        Long long1;
        Boolean bool1 = false;
        int i = paramInt;
        if (paramCursor.isNull(i)) {
            long1 = null;
        } else {
            long1 = paramCursor.getLong(i);
        }
        i = paramCursor.getInt(paramInt + 1);
        int j = paramInt + 2;
        if (paramCursor.isNull(j)) {
            str1 = null;
        } else {
            str1 = paramCursor.getString(j);
        }
        j = paramInt + 3;
        if (paramCursor.isNull(j)) {
            str2 = null;
        } else {
            str2 = paramCursor.getString(j);
        }
        float f1 = paramCursor.getFloat(paramInt + 4);
        float f2 = paramCursor.getFloat(paramInt + 5);
        j = paramInt + 6;
        if (paramCursor.isNull(j)) {
            long2 = null;
        } else {
            long2 = paramCursor.getLong(j);
        }
        j = paramCursor.getInt(paramInt + 7);
        float f3 = paramCursor.getFloat(paramInt + 8);
        int k = paramInt + 9;
        boolean bool = paramCursor.isNull(k);
        boolean bool2 = true;
        if (bool) {
            bool3 = null;
        } else {
            if (paramCursor.getShort(k) != 0) {
                bool = true;
            } else {
                bool = false;
            }
            bool3 = bool;
        }
        k = paramInt + 10;
        if (paramCursor.isNull(k)) {
            bool4 = null;
        } else {
            if (paramCursor.getShort(k) != 0) {
                bool = true;
            } else {
                bool = false;
            }
            bool4 = bool;
        }
        paramInt += 11;
        if (paramCursor.isNull(paramInt)) {
            paramCursor = null;
        } else {
            if (paramCursor.getShort(paramInt) != 0) {
                bool = bool2;
            } else {
                bool = false;
            }
            bool1 = bool;
        }
        return new GroupModel(long1, i, str1, str2, f1, f2, long2, j, f3, bool3, bool4, bool1);
    }

    @Override
    public void readEntity(Cursor paramCursor, GroupModel paramGroupModel, int paramInt) {
        Boolean bool3;
        Boolean bool1 = false;
        Long aLong;
        String cursorString;
        int i = paramInt;
        boolean bool = paramCursor.isNull(i);
        Cursor cursor = null;
        if (bool) {
            aLong = null;
        } else {
            aLong = paramCursor.getLong(i);
        }
        paramGroupModel.setId(aLong);
        paramGroupModel.setStyle(paramCursor.getInt(paramInt + 1));
        i = paramInt + 2;
        if (paramCursor.isNull(i)) {
            cursorString = null;
        } else {
            cursorString = paramCursor.getString(i);
        }
        paramGroupModel.setGroupId(cursorString);
        i = paramInt + 3;
        if (paramCursor.isNull(i)) {
            cursorString = null;
        } else {
            cursorString = paramCursor.getString(i);
        }
        paramGroupModel.setGroupName(cursorString);
        paramGroupModel.setColorTemp(paramCursor.getFloat(paramInt + 4));
        paramGroupModel.setBrightness(paramCursor.getFloat(paramInt + 5));
        i = paramInt + 6;
        if (paramCursor.isNull(i)) {
            aLong = null;
        } else {
            aLong = paramCursor.getLong(i);
        }
        paramGroupModel.setTimeInterval(aLong);
        paramGroupModel.setFeaturesIndex(paramCursor.getInt(paramInt + 7));
        paramGroupModel.setSpeed(paramCursor.getFloat(paramInt + 8));
        i = paramInt + 9;
        bool = paramCursor.isNull(i);
        boolean bool2 = true;
        if (bool) {
            bool3 = null;
        } else {
            if (paramCursor.getShort(i) != 0) {
                bool = true;
            } else {
                bool = false;
            }
            bool3 = bool;
        }
        paramGroupModel.setIsSelected(bool3);
        i = paramInt + 10;
        if (paramCursor.isNull(i)) {
            bool3 = null;
        } else {
            if (paramCursor.getShort(i) != 0) {
                bool = true;
            } else {
                bool = false;
            }
            bool3 = bool;
        }
        paramGroupModel.setIsOn(bool3);
        paramInt += 11;
        if (paramCursor.isNull(paramInt)) {
            paramCursor = cursor;
        } else {
            if (paramCursor.getShort(paramInt) != 0) {
                bool = bool2;
            } else {
                bool = false;
            }
            bool1 = bool;
        }
        paramGroupModel.setSpread(bool1);
    }

    @Override
    public Long readKey(Cursor paramCursor, int paramInt) {
        paramInt += 0;
        return paramCursor.isNull(paramInt) ? null : paramCursor.getLong(paramInt);
    }

    @Override
    protected final Long updateKeyAfterInsert(GroupModel paramGroupModel, long paramLong) {
        paramGroupModel.setId(paramLong);
        return paramLong;
    }

    public static class Properties {
        public static final Property Brightness;

        public static final Property ColorTemp;

        public static final Property FeaturesIndex;

        public static final Property GroupId;

        public static final Property GroupName;

        public static final Property Id = new Property(0, Long.class, "id", true, "_id");

        public static final Property IsOn;

        public static final Property IsSelected;

        public static final Property Speed;

        public static final Property Spread;

        public static final Property Style = new Property(1, int.class, "style", false, "STYLE");

        public static final Property TimeInterval;

        static {
            GroupId = new Property(2, String.class, "groupId", false, "GROUP_ID");
            GroupName = new Property(3, String.class, "groupName", false, "GROUP_NAME");
            ColorTemp = new Property(4, float.class, "colorTemp", false, "COLOR_TEMP");
            Brightness = new Property(5, float.class, "brightness", false, "BRIGHTNESS");
            TimeInterval = new Property(6, Long.class, "timeInterval", false, "TIME_INTERVAL");
            FeaturesIndex = new Property(7, int.class, "featuresIndex", false, "FEATURES_INDEX");
            Speed = new Property(8, float.class, "speed", false, "SPEED");
            IsSelected = new Property(9, Boolean.class, "isSelected", false, "IS_SELECTED");
            IsOn = new Property(10, Boolean.class, "isOn", false, "IS_ON");
            Spread = new Property(11, Boolean.class, "spread", false, "SPREAD");
        }
    }
}
