package com.onmicro.snc_ble.core.bluetooth;

public class Util {
    public static final int BLE_CHANNEL_INDEX = 37;

    public static final int CRC_LENGTH = 2;

    public static final int PDU_EXHEADER_LENGTH = 13;

    public static final int PREAMBLE_LENGTH = 3;

    public static short check_crc16(char[] paramArrayOfChar1, int paramInt1, char[] paramArrayOfChar2, int paramInt2) {
        int j = 0;
        int i = -1;
        while (j < paramInt1) {
            i = (short)(i ^ paramArrayOfChar1[paramInt1 - 1 - j] << '\b');
            for (int k = 0; k < 8; k = (char) (k + 1)) {
                if ((i & 0x8000) != 0) {
                    i = (short) (i << 1 ^ 0x1021);
                } else {
                    i = (short) (i << 1);
                }
            }
            j = (char) (j + 1);
        }
        for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1 = (char)(paramInt1 + 1)) {
            i = (short)(invert_8(paramArrayOfChar2[paramInt1]) << '\b' ^ i);
            for (j = 0; j < 8; j = (char)(j + 1)) {
                if ((i & 0x8000) != 0) {
                    i = (short)(i << 1 ^ 0x1021);
                } else {
                    i = (short)(i << 1);
                }
            }
        }
        return (short)(invert_16((short) i) ^ 0xFFFF);
    }

    public static void get_rf_payload(char[] paramArrayOfChar1, int paramInt1, char[] paramArrayOfChar2, int paramInt2, char[] paramArrayOfChar3) {
        int[] arrayOfInt1 = new int[7];
        int[] arrayOfInt2 = new int[7];
        whitening_init(37, arrayOfInt1);
        whitening_init(63, arrayOfInt2);
        int m = paramInt1 + 16;
        int j = m + paramInt2;
        int k = j + 2;
        char[] arrayOfChar = new char[k];
        arrayOfChar[13] = 'q';
        arrayOfChar[14] = '\017';
        arrayOfChar[15] = 'U';
        int i;
        for (i = 0; i < paramInt1; i++)
            arrayOfChar[i + 16] = paramArrayOfChar1[paramInt1 - i - 1];
        for (i = 0; i < paramInt2; i++)
            arrayOfChar[m + i] = paramArrayOfChar2[i];
        i = 0;
        while (true) {
            m = paramInt1 + 3;
            if (i < m) {
                m = i + 13;
                arrayOfChar[m] = invert_8(arrayOfChar[m]);
                i++;
                continue;
            }
            break;
        }
        i = check_crc16(paramArrayOfChar1, paramInt1, paramArrayOfChar2, paramInt2);
        arrayOfChar[j + 0] = (char)(i & 0xFF);
        arrayOfChar[j + 1] = (char)(i >> 8 & 0xFF);
        whitening_encode(arrayOfChar, paramInt1 + paramInt2 + 2, arrayOfInt2);
        whitening_encode(arrayOfChar, k, arrayOfInt1);
        System.arraycopy(arrayOfChar, 13, paramArrayOfChar3, 0, m + paramInt2 + 2);
    }

    public static String hexString(char[] paramArrayOfChar) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b = 0; b < paramArrayOfChar.length; b++) {
            stringBuffer.append(String.format("%02X ", (byte) paramArrayOfChar[b]));
        }
        return stringBuffer.toString();
    }

    public static short invert_16(short paramShort) {
        char m = Character.MIN_VALUE;
        short k = 0;
        for (int i = 0; m < 16; i = k) {
            if ((1 << m & paramShort) != 0) {
                k = (short) (i | 1 << 15 - m);
            }
            m = (char) (m + 1);
        }
        return k;
    }

    public static char invert_8(char paramChar) {
        char m = Character.MIN_VALUE;
        char k = 0;
        for (int i = 0; m < 8; i = k) {
            if ((1 << m & paramChar) != 0) {
                k = (char) (i | 1 << 7 - m);
            }
            m = (char) (m + 1);
        }
        return k;
    }

    public static void whitening_encode(char[] paramArrayOfChar, int paramInt, int[] paramArrayOfInt) {
        int i = 0;
        while (i < paramInt) {
            int j = paramArrayOfChar.length;
            int m = paramInt - i;
            int n = paramArrayOfChar[(j - m)];
            j = 0;
            int k = 0;
            while (j < 8) {
                k += ((n >> j & 0x1 ^ whitening_output(paramArrayOfInt)) << j);
                j += 1;
            }
            paramArrayOfChar[(paramArrayOfChar.length - m)] = ((char) k);
            i += 1;
        }
    }

    public static void whitening_init(int paramInt, int[] paramArrayOfInt) {
        paramArrayOfInt[0] = 1;
        int i = 1;
        while (i < 7) {
            paramArrayOfInt[i] = (paramInt >> 6 - i & 0x1);
            i += 1;
        }
    }

    public static int whitening_output(int[] paramArrayOfInt) {
        int i = paramArrayOfInt[3];
        int j = paramArrayOfInt[6];
        paramArrayOfInt[3] = paramArrayOfInt[2];
        paramArrayOfInt[2] = paramArrayOfInt[1];
        paramArrayOfInt[1] = paramArrayOfInt[0];
        paramArrayOfInt[0] = paramArrayOfInt[6];
        paramArrayOfInt[6] = paramArrayOfInt[5];
        paramArrayOfInt[5] = paramArrayOfInt[4];
        paramArrayOfInt[4] = i ^ j;
        return paramArrayOfInt[0];
    }
}
