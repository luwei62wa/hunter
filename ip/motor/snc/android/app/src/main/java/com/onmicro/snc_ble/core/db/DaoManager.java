
package com.onmicro.snc_ble.core.db;

import android.content.Context;

import com.onmicro.snc_ble.core.db.dao.DaoMaster;
import com.onmicro.snc_ble.core.db.dao.DaoSession;

import org.greenrobot.greendao.query.QueryBuilder;

public class DaoManager {
    private static final String DB_NAME = "db.sqlite";

    private static final String TAG = "DaoManager";

    private static DaoManager instance;

    private DaoManager() {
    }

    public static DaoManager getInstance() {
        if (instance == null) {
            synchronized (DaoManager.class) {
                if (instance == null) {
                    instance = new DaoManager();
                }
            }
        }
        return instance;
    }

    private DaoMaster mDaoMaster;

    private DaoSession mDaoSession;

    private DaoMaster.DevOpenHelper mHelper;

    static {

    }

    public void clear() {
        if (mDaoSession != null) {
            mDaoSession.clear();
            mDaoSession = null;
        }
    }

    public void close() {
        clear();
        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }
    }

    public DaoMaster getDaoMaster() {
        return mDaoMaster;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public void init(Context paramContext) {
        mHelper = new DaoMaster.DevOpenHelper(paramContext, "db.sqlite", null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = mDaoMaster.newSession();
    }

    public void init(Context paramContext, String paramString) {
        mHelper = new DaoMaster.DevOpenHelper(paramContext, paramString, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = this.mDaoMaster.newSession();
    }

    public void setDebug() {
        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;
    }
}
