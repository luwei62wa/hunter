package com.onmicro.snc_ble.adpter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.macro.Macro;
import com.onmicro.snc_ble.models.GroupModel;

/**
 * @author peng
 */
public class GroupedTabBarItemAdapter extends BaseQuickAdapter<GroupModel, BaseViewHolder> {
    private Context context;

    public GroupedTabBarItemAdapter(Context paramContext) {
        super(R.layout.row_grouped_tabbar_item);
        context = paramContext;
    }

    @Override
    protected void convert(BaseViewHolder paramBaseViewHolder, GroupModel paramGroupModel) {
        int sel;
        int nor;
        paramBaseViewHolder.itemView.getLayoutParams().width = (int) (Macro.SCREEN_WIDTH / 4.0D);
        paramBaseViewHolder.setText(R.id.title_text, paramGroupModel.getGroupName());
        switch (paramGroupModel.getStyle()) {
            default:
                // 7
                nor = R.mipmap.tabbar_bedroom_nor;
                sel = R.mipmap.tabbar_bedroom_sel;
                break;
            case GroupModel.GROUPED_STYLE_ADD_GROUP:
                paramBaseViewHolder.setText(R.id.title_text, context.getString(R.string.group_add));
                nor = R.mipmap.tabbar_addgroup_nor;
                sel = R.mipmap.tabbar_addgroup_sel;
                break;
            case GroupModel.GROUPED_STYLE_MASTER_CONTROL:
                paramBaseViewHolder.setText(R.id.title_text, context.getString(R.string.group_control));
                nor = R.mipmap.tabbar_mastercontrol_nor;
                sel = R.mipmap.tabbar_mastercontrol_sel;
                break;
            case GroupModel.GROUPED_STYLE_ADD_FAN:
                nor = R.mipmap.tabbar_fan_nor;
                sel = R.mipmap.tabbar_fan_sel;
                break;
            case GroupModel.GROUPED_STYLE_GUESTWC:
                nor = R.mipmap.tabbar_wc_nor;
                sel = R.mipmap.tabbar_wc_sel;
                break;
            case GroupModel.GROUPED_STYLE_RESTAURANT:
                nor = R.mipmap.tabbar_restaurant_nor;
                sel = R.mipmap.tabbar_restaurant_sel;
                break;
            case GroupModel.GROUPED_STYLE_KITCHEN:
                nor = R.mipmap.tabbar_kitchen_nor;
                sel = R.mipmap.tabbar_kitchen_sel;
                break;
            case GroupModel.GROUPED_STYLE_STUDY:
                nor = R.mipmap.tabbar_study_nor;
                sel = R.mipmap.tabbar_study_sel;
                break;
            case GroupModel.GROUPED_STYLE_GUEST_ROOM:
                nor = R.mipmap.tabbar_guestroom_nor;
                sel = R.mipmap.tabbar_guestroom_sel;
                break;
            case GroupModel.GROUPED_STYLE_MASTER_BEDROOM:
                nor = R.mipmap.tabbar_masterbedroom_nor;
                sel = R.mipmap.tabbar_masterbedroom_sel;
                break;
            case GroupModel.GROUPED_STYLE_WIVIN_GROOM:
                nor = R.mipmap.tabbar_wivingroom_nor;
                sel = R.mipmap.tabbar_wivingroom_sel;
                break;
        }
        if (paramGroupModel.getIsSelected()) {
            paramBaseViewHolder.setImageResource(R.id.imageview, sel);
            paramBaseViewHolder.setTextColor(R.id.title_text, -1);
            return;
        }
        paramBaseViewHolder.setImageResource(R.id.imageview, nor);
        paramBaseViewHolder.setTextColor(R.id.title_text, -12932421);
    }
}
