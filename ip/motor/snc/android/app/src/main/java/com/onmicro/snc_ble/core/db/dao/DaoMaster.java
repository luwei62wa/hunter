package com.onmicro.snc_ble.core.db.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.AbstractDaoMaster;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseOpenHelper;
import org.greenrobot.greendao.database.StandardDatabase;
import org.greenrobot.greendao.identityscope.IdentityScopeType;

public class DaoMaster extends AbstractDaoMaster {
    public static final int SCHEMA_VERSION = 1;

    public DaoMaster(SQLiteDatabase paramSQLiteDatabase) {
        this(new StandardDatabase(paramSQLiteDatabase));
    }

    public DaoMaster(Database paramDatabase) {
        super(paramDatabase, 1);
        registerDaoClass(GroupModelDao.class);
    }

    public static void createAllTables(Database paramDatabase, boolean paramBoolean) {
        GroupModelDao.createTable(paramDatabase, paramBoolean);
    }

    public static void dropAllTables(Database paramDatabase, boolean paramBoolean) {
        GroupModelDao.dropTable(paramDatabase, paramBoolean);
    }

    public static DaoSession newDevSession(Context paramContext, String paramString) {
        return (new DaoMaster((new DevOpenHelper(paramContext, paramString)).getWritableDb())).newSession();
    }

    @Override
    public DaoSession newSession() {
        return new DaoSession(this.db, IdentityScopeType.Session, this.daoConfigMap);
    }

    @Override
    public DaoSession newSession(IdentityScopeType paramIdentityScopeType) {
        return new DaoSession(this.db, paramIdentityScopeType, this.daoConfigMap);
    }

    public static class DevOpenHelper extends OpenHelper {
        public DevOpenHelper(Context param1Context, String param1String) {
            super(param1Context, param1String);
        }

        public DevOpenHelper(Context param1Context, String param1String, SQLiteDatabase.CursorFactory param1CursorFactory) {
            super(param1Context, param1String, param1CursorFactory);
        }

        @Override
        public void onUpgrade(Database param1Database, int param1Int1, int param1Int2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Upgrading schema from version ");
            stringBuilder.append(param1Int1);
            stringBuilder.append(" to ");
            stringBuilder.append(param1Int2);
            stringBuilder.append(" by dropping all tables");
            Log.i("greenDAO", stringBuilder.toString());
            DaoMaster.dropAllTables(param1Database, true);
            onCreate(param1Database);
        }
    }

    public static abstract class OpenHelper extends DatabaseOpenHelper {
        public OpenHelper(Context param1Context, String param1String) {
            super(param1Context, param1String, 1);
        }

        public OpenHelper(Context param1Context, String param1String, SQLiteDatabase.CursorFactory param1CursorFactory) {
            super(param1Context, param1String, param1CursorFactory, 1);
        }

        @Override
        public void onCreate(Database param1Database) {
            Log.i("greenDAO", "Creating tables for schema version 1");
            DaoMaster.createAllTables(param1Database, false);
        }
    }
}
