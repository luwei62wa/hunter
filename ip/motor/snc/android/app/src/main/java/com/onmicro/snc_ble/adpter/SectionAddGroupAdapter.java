package com.onmicro.snc_ble.adpter;

import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.macro.Macro;
import com.onmicro.snc_ble.models.SectionAddGroup;

import java.util.List;

public class SectionAddGroupAdapter extends BaseSectionQuickAdapter<SectionAddGroup, BaseViewHolder> {
    public SectionAddGroupAdapter(int paramInt1, int paramInt2, List paramList) {
        super(paramInt1, paramInt2, paramList);
    }

    @Override
    protected void convert(BaseViewHolder paramBaseViewHolder, SectionAddGroup paramSectionAddGroup) {
        ViewGroup.LayoutParams layoutParams = paramBaseViewHolder.itemView.getLayoutParams();
        layoutParams.width = (int) (Macro.SCREEN_WIDTH / 3.0D);
        layoutParams.height = (int) (Macro.SCREEN_WIDTH / 3.0D);
        paramBaseViewHolder.setText(R.id.title_text, paramSectionAddGroup.t.getGroupName());
        if (paramSectionAddGroup.t.getSelected()) {
            paramBaseViewHolder.setImageResource(R.id.imageview, R.mipmap.add_group_selected_sel);
            return;
        }
        paramBaseViewHolder.setImageResource(R.id.imageview, R.mipmap.add_group_selected_nor);
    }

    @Override
    protected void convertHead(BaseViewHolder paramBaseViewHolder, SectionAddGroup paramSectionAddGroup) {
        paramBaseViewHolder.setText(R.id.header_text, paramSectionAddGroup.header);
    }
}
