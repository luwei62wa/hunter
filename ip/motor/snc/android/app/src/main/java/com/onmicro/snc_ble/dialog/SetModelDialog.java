package com.onmicro.snc_ble.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.app.App;
import com.onmicro.snc_ble.common.PreferenceUtils;
import com.onmicro.snc_ble.core.db.DBGroup;

/**
 * @author PengYaQuan
 * @create 2019-11-01 15:34
 * @Describe
 */
public class SetModelDialog extends Dialog implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    public static final int CUSTOM_IMAGE_TYPE = 4;

    public static final int ERROR_TYPE = 1;

    public static final int NORMAL_TYPE = 0;

    public static final int PROGRESS_TYPE = 5;

    public static final int SUCCESS_TYPE = 2;

    public static final int WARNING_TYPE = 3;

    public static final String MESH_MODEL = "mesh";
    public static final String TRADITIONAL_MODEL = "2.4G";


    private int mAlertType;

    private Button mCancelButton;

    private SetModelDialog.OnClickListener mCancelClickListener;

    private String mCancelText;

    private boolean mCloseFromCancel;

    private Button mConfirmButton;

    private SetModelDialog.OnClickListener mConfirmClickListener;

    private String mConfirmText;

    private String mContentText;

    private View mDialogView;

    private AnimationSet mModalInAnim;

    private AnimationSet mModalOutAnim;

    private Animation mOverlayOutAnim;

    private boolean mShowCancel;

    private boolean mShowContent;

    private String mTitleText;

    private String mChooseModel;

    private String mPreferenceModel;

    private TextView mTitleTextView;

    public SetModelDialog(Context paramContext) {
        this(paramContext, 0);
    }

    public SetModelDialog(Context paramContext, int paramInt) {
        super(paramContext, R.style.sweet_alert_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        mAlertType = paramInt;
        mModalInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_in);
        mModalOutAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_out);
        mModalOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation param1Animation) {
                mDialogView.setVisibility(View.GONE);
                mDialogView.post(() -> {
                    if (mCloseFromCancel) {
                        cancel();
                        return;
                    }
                    dismiss();
                });
            }

            @Override
            public void onAnimationRepeat(Animation param1Animation) {
            }

            @Override
            public void onAnimationStart(Animation param1Animation) {
            }
        });

        mOverlayOutAnim = new Animation() {
            @Override
            protected void applyTransformation(float param1Float, Transformation param1Transformation) {
                Window window = getWindow();
                if (window != null) {
                    WindowManager.LayoutParams layoutParams = window.getAttributes();
                    layoutParams.alpha = 1.0F - param1Float;
                    window.setAttributes(layoutParams);
                }
            }
        };
        mOverlayOutAnim.setDuration(120L);
    }

    private void changeAlertType(int paramInt, boolean paramBoolean) {
        mAlertType = paramInt;
        if (mDialogView != null) {
            if (!paramBoolean)
                restore();
            // ???? -peng
//            switch (mAlertType) {
//
//            }
        }
    }

    private void dismissWithAnimation(boolean paramBoolean) {
        mCloseFromCancel = paramBoolean;
        mConfirmButton.startAnimation(mOverlayOutAnim);
        mDialogView.startAnimation(mModalOutAnim);
    }

    private void restore() {
        mConfirmButton.setVisibility(View.VISIBLE);
        mConfirmButton.setBackgroundResource(R.drawable.dialog_alert_left_btn_bg);
    }

    @Override
    public void cancel() {
        dismissWithAnimation(true);
    }

    public void changeAlertType(int paramInt) {
        changeAlertType(paramInt, false);
    }

    public void dismissWithAnimation() {
        dismissWithAnimation(false);
    }

    public int getAlerType() {
        return this.mAlertType;
    }

    public String getCancelText() {
        return this.mCancelText;
    }

    public String getConfirmText() {
        return this.mConfirmText;
    }

    public String getContentText() {
        return this.mContentText;
    }

    public String getTitleText() {
        return this.mTitleText;
    }

    public boolean isShowCancelButton() {
        return this.mShowCancel;
    }

    public boolean isShowContentText() {
        return this.mShowContent;
    }

    @Override
    public void onClick(View paramView) {
        if (paramView.getId() == R.id.cancel_button) {
            if (mCancelClickListener != null)
                mCancelClickListener.onClick(mChooseModel);
            dismissWithAnimation();
            return;
        }
        if (paramView.getId() == R.id.confirm_button) {
            if (mConfirmClickListener != null) {
                if (!mPreferenceModel.equals(mChooseModel)) {
                    DBGroup.deleteAll();
                    PreferenceUtils.putIntValue(App.getInstance(), "RUN_COUNT", 0);
                }
                mConfirmClickListener.onClick(mChooseModel);
            }
            dismissWithAnimation();
        }
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.dialog_setting);
        if (getWindow() != null) {
            this.mDialogView = getWindow().getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
        }


        this.mTitleTextView = findViewById(R.id.title_text);
//        this.mContentTextView = findViewById(R.id.content_text);
        this.mConfirmButton = findViewById(R.id.confirm_button);
        this.mCancelButton = findViewById(R.id.cancel_button);
        RadioGroup radioGroup = findViewById(R.id.radio_group);
        RadioButton radioMesh = findViewById(R.id.radio_mesh);
        RadioButton radioG = findViewById(R.id.radio_g);


        mChooseModel = PreferenceUtils.getValue(App.getInstance(), "supportModel");
        mPreferenceModel = mChooseModel;
        if (mChooseModel.isEmpty() || mChooseModel.equals(MESH_MODEL)) {
            radioMesh.setChecked(true);
            radioG.setChecked(false);
        } else if (mChooseModel.equals(TRADITIONAL_MODEL)) {
            radioG.setChecked(true);
            radioMesh.setChecked(false);
        }


        this.mConfirmButton.setOnClickListener(this);
        this.mCancelButton.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
        setTitleText(mTitleText);
        setCancelText(mCancelText);
        setConfirmText(mConfirmText);
        changeAlertType(mAlertType, true);
    }

    @Override
    protected void onStart() {
        this.mDialogView.startAnimation(mModalInAnim);
    }

    public SetModelDialog setCancelClickListener(SetModelDialog.OnClickListener paramOnClickListener) {
        mCancelClickListener = paramOnClickListener;
        return this;
    }

    public SetModelDialog setCancelText(String paramString) {
        mCancelText = paramString;
        if (mCancelButton != null && mCancelText != null) {
            showCancelButton(true);
            mCancelButton.setText(mCancelText);
        }
        return this;
    }

    public SetModelDialog setConfirmClickListener(SetModelDialog.OnClickListener paramOnClickListener) {
        mConfirmClickListener = paramOnClickListener;
        return this;
    }

    public SetModelDialog setConfirmText(String paramString) {
        mConfirmText = paramString;
        if (mConfirmButton != null && mConfirmText != null)
            mConfirmButton.setText(mConfirmText);
        return this;
    }

//    public SetModelDialog setContentText(String paramString) {
//        mContentText = paramString;
//        if (mContentTextView != null && mContentText != null) {
//            showContentText(true);
//            mContentTextView.setText(mContentText);
//        }
//        return this;
//    }

    public SetModelDialog setTitleText(String paramString) {
        mTitleText = paramString;
        if (mTitleTextView != null && mTitleText != null)
            mTitleTextView.setText(mTitleText);
        return this;
    }

    public SetModelDialog showCancelButton(boolean paramBoolean) {
        mShowCancel = paramBoolean;
        if (mCancelButton != null) {
            if (mShowCancel) {
                mCancelButton.setVisibility(View.VISIBLE);
            } else {
                mCancelButton.setVisibility(View.GONE);
            }
        }
        return this;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        PreferenceUtils.putBooleanValue(App.getInstance(), "checkbox_show", true);
        switch (checkedId) {
            case R.id.radio_mesh:
                mChooseModel = MESH_MODEL;
                break;
            default:
                mChooseModel = TRADITIONAL_MODEL;
                break;
        }
    }

//    public SetModelDialog showContentText(boolean paramBoolean) {
//        mShowContent = paramBoolean;
//        if (mContentTextView != null) {
//            if (mShowContent) {
//                mContentTextView.setVisibility(View.VISIBLE);
//            } else {
//                mContentTextView.setVisibility(View.GONE);
//            }
//        }
//        return this;
//    }

    public static interface OnClickListener {
        void onClick(String mode);
    }
}
