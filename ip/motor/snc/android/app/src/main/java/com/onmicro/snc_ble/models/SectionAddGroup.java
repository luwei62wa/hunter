package com.onmicro.snc_ble.models;

import com.chad.library.adapter.base.entity.SectionEntity;

public class SectionAddGroup extends SectionEntity<AddGroupModel> {
    public SectionAddGroup(AddGroupModel paramAddGroupModel) {
        super(paramAddGroupModel);
    }

    public SectionAddGroup(boolean paramBoolean, String paramString) {
        super(paramBoolean, paramString);
    }
}
