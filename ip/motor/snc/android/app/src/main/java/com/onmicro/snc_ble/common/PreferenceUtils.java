package com.onmicro.snc_ble.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class PreferenceUtils {
    public static final void RemoveValue(Context paramContext, String paramString) {
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.remove(paramString);
        if (!editor.commit()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("save ");
            stringBuilder.append(paramString);
            stringBuilder.append(" failed");
            Log.e("移除Shared", stringBuilder.toString());
        }
    }

    public static final Boolean getBooleanValue(Context paramContext, String paramString) {
        return getSharedPreference(paramContext).getBoolean(paramString, true);
    }

    public static final int getIntValue(Context paramContext, String paramString) {
        return getSharedPreference(paramContext).getInt(paramString, 0);
    }

    public static final long getLongValue(Context paramContext, String paramString, long paramLong) {
        return getSharedPreference(paramContext).getLong(paramString, paramLong);
    }

    private static final SharedPreferences getSharedPreference(Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext);
    }

    public static final String getValue(Context paramContext, String paramString) {
        return getSharedPreference(paramContext).getString(paramString, "");
    }

    public static final Boolean hasValue(Context paramContext, String paramString) {
        return getSharedPreference(paramContext).contains(paramString);
    }

    public static final boolean putBooleanValue(Context paramContext, String paramString, boolean paramBoolean) {
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.putBoolean(paramString, paramBoolean);
        return editor.commit();
    }

    public static final boolean putIntValue(Context paramContext, String paramString, int paramInt) {
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.putInt(paramString, paramInt);
        return editor.commit();
    }

    public static final boolean putLongValue(Context paramContext, String paramString, Long paramLong) {
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.putLong(paramString, paramLong);
        return editor.commit();
    }

    public static final boolean putValue(Context paramContext, String paramString1, String paramString2) {
        String str = paramString2;
        if (paramString2 == null)
            str = "";
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.putString(paramString1, str);
        return editor.commit();
    }
}
