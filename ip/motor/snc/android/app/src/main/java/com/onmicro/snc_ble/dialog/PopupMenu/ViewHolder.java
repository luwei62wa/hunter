package com.onmicro.snc_ble.dialog.PopupMenu;

import android.util.SparseArray;
import android.view.View;

/**
 * @author peng
 */
public class ViewHolder {
    public static <T extends View> T get(View paramView, int paramInt) {
        SparseArray sparseArray2 = (SparseArray)paramView.getTag();
        SparseArray sparseArray1 = sparseArray2;
        if (sparseArray2 == null) {
            sparseArray1 = new SparseArray();
            paramView.setTag(sparseArray1);
        }
        View view2 = (View)sparseArray1.get(paramInt);
        View view1 = view2;
        if (view2 == null) {
            view1 = paramView.findViewById(paramInt);
            sparseArray1.put(paramInt, view1);
        }
        return (T)view1;
    }
}
