package com.onmicro.snc_ble.view;

import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageButton;

public class UIImageButton extends AppCompatImageButton {
    public final float[] BUTTON_PRESSED = {
            1.0F, 0.0F, 0.0F, 0.0F, -50.0F, 0.0F, 1.0F, 0.0F, 0.0F, -50.0F,
            0.0F, 0.0F, 1.0F, 0.0F, -50.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F };

    public final float[] BUTTON_RELEASED = {
            1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F,
            0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F };

    public UIImageButton(Context paramContext) {
        super(paramContext);
        initView();
    }

    public UIImageButton(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initView();
    }

    public UIImageButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        initView();
    }

    private void initView() {}

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setOnTouchListener(new OnTouchListener());
    }

    class OnTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
            if (param1MotionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                getDrawable().setColorFilter(new ColorMatrixColorFilter(BUTTON_PRESSED));
                setImageDrawable(getDrawable());
            } else if (param1MotionEvent.getAction() == MotionEvent.ACTION_UP) {
                getDrawable().setColorFilter(new ColorMatrixColorFilter(BUTTON_RELEASED));
                setImageDrawable(getDrawable());
            }
            return false;
        }
    }
}
