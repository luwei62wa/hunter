package com.onmicro.snc_ble.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.preference.PreferenceManager;

import java.util.Locale;

public class LocalizedUtils {
    public static final String USER_LANGUAGE = "user_language";

    private static LocalizedUtils instance;

    private static Context mContext;
    private LocalizedUtils() {
    }

    public static LocalizedUtils getInstance() {
        if (instance == null) {
            synchronized (LocalizedUtils.class) {
                if (instance == null) {
                    instance = new LocalizedUtils();
                }
            }
        }
        return instance;
    }

    public static Context attachBaseContext(Context paramContext) {
        getInstance().init(paramContext);
        setUserLanguage(currentLanguage());
        return paramContext;
    }

    @TargetApi(24)
    private static Context createConfigurationResources(Context paramContext) {
        Configuration configuration = paramContext.getResources().getConfiguration();
        configuration.setLocale(createConstant(currentLanguage()));
        return paramContext.createConfigurationContext(configuration);
    }

    public static Locale createConstant(String paramString) {
        if (paramString.length() == 0)
            return getSysLocale();
        String[] arrayOfString = paramString.split("_");
        return (arrayOfString.length == 1) ? new Locale(arrayOfString[0]) : new Locale(arrayOfString[0], arrayOfString[1]);
    }

    public static String currentLanguage() {
        String userLanguage = getValue(mContext, USER_LANGUAGE);
        if (userLanguage.length() == 0) {
            Locale locale = getSysLocale();
            return (locale.getCountry().length() == 0) ? locale.getLanguage() : String.format("%s_%s", locale.getLanguage(), locale.getCountry());
        }
        return userLanguage;
    }

    private static SharedPreferences getSharedPreference(Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext);
    }

    public static Locale getSysLocale() {
        //LocaleList.getDefault().get(0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return LocaleList.getDefault().get(0);
        } else {
            return Locale.getDefault();
        }
    }

    private static String getValue(Context paramContext, String paramString) {
        return getSharedPreference(paramContext).getString(paramString, "");
    }

    private static boolean putValue(Context paramContext, String paramString1, String paramString2) {
        String str = paramString2;
        if (paramString2 == null)
            str = "";
        SharedPreferences.Editor editor = getSharedPreference(paramContext).edit();
        editor.putString(paramString1, str);
        return editor.commit();
    }

    public static void setUserLanguage(String paramString) {
        Locale locale = createConstant(paramString);
        Configuration configuration = mContext.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        Resources resources = mContext.getResources();
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        putValue(mContext, "user_language", paramString);
    }

    public Context getContext() {
        return mContext;
    }

    public void init(Context paramContext) {
        mContext = paramContext;
    }
}
