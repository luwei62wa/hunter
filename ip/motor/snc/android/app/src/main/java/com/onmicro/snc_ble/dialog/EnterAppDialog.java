package com.onmicro.snc_ble.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.onmicro.snc_ble.R;
import com.onmicro.snc_ble.app.App;
import com.onmicro.snc_ble.common.PreferenceUtils;


public class EnterAppDialog extends Dialog implements View.OnClickListener {

    private int mAlertType;

    private boolean mCloseFromCancel;

    private Button mConfirmButton;

    private OnClickListener mConfirmClickListener;

    private String mContentText;

    private TextView mContentTextView;

    private View mDialogView;

    private AnimationSet mModalInAnim;

    private AnimationSet mModalOutAnim;

    private Animation mOverlayOutAnim;

    private boolean mShowContent;

    private TextView mTitleTextView;

    public EnterAppDialog(Context paramContext) {
        this(paramContext, 0);
    }

    public EnterAppDialog(Context paramContext, int paramInt) {
        super(paramContext, R.style.sweet_alert_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        mAlertType = paramInt;
        mModalInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_in);
        mModalOutAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_out);
        mModalOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation param1Animation) {
                mDialogView.setVisibility(View.GONE);
                mDialogView.post(() -> {
                    if (mCloseFromCancel) {
                        cancel();
                        return;
                    }
                    dismiss();
                });
            }

            @Override
            public void onAnimationRepeat(Animation param1Animation) {
            }

            @Override
            public void onAnimationStart(Animation param1Animation) {
            }
        });

        mOverlayOutAnim = new Animation() {
            @Override
            protected void applyTransformation(float param1Float, Transformation param1Transformation) {
                Window window = getWindow();
                if (window != null) {
                    WindowManager.LayoutParams layoutParams = window.getAttributes();
                    layoutParams.alpha = 1.0F - param1Float;
                    window.setAttributes(layoutParams);
                }
            }
        };
        mOverlayOutAnim.setDuration(120L);
    }

    private void changeAlertType(int paramInt, boolean paramBoolean) {
        mAlertType = paramInt;
        if (mDialogView != null) {
            if (!paramBoolean)
                restore();
        }
    }

    private void dismissWithAnimation(boolean paramBoolean) {
        mCloseFromCancel = paramBoolean;
        mConfirmButton.startAnimation(mOverlayOutAnim);
        mDialogView.startAnimation(mModalOutAnim);
    }

    private void restore() {
        mConfirmButton.setVisibility(View.VISIBLE);
        mConfirmButton.setBackgroundResource(R.drawable.dialog_alert_left_btn_bg);
    }

    @Override
    public void cancel() {
        dismissWithAnimation(true);
    }

    public void dismissWithAnimation() {
        dismissWithAnimation(false);
    }

    @Override
    public void onClick(View paramView) {
        if (paramView.getId() == R.id.confirm_button) {
            dismissWithAnimation();
        }
    }

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.dialog_enter_app);
        if (getWindow() != null) {
            this.mDialogView = getWindow().getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
        }
        mTitleTextView = findViewById(R.id.title_text);
        mContentTextView = findViewById(R.id.content);
        mConfirmButton = findViewById(R.id.confirm_button);
        CheckBox checkBox = findViewById(R.id.cb_display);
        mConfirmButton.setOnClickListener(this);
        setContentText(mContentText);
        changeAlertType(mAlertType, true);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            PreferenceUtils.putBooleanValue(App.getInstance(), "checkbox_show", !isChecked);
        });
    }

    @Override
    protected void onStart() {
        this.mDialogView.startAnimation(mModalInAnim);
    }

    public EnterAppDialog setContentText(String paramString) {
        mContentText = paramString;
        if (mContentTextView != null && mContentText != null) {
            mContentTextView.setText(mContentText);
        }
        return this;
    }
}
