package com.onmicro.snc_ble.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.onmicro.snc_ble.R;

/**
 * @author PengYaQuan
 * @create 2019-10-22 14:11
 * @Describe 自定义滑动动画
 */
public class ColorPickerView extends ConstraintLayout {
    public static final int SATURATION_MAX = 1000;
    private float mColorTemp = 0.0F;
    private boolean isChanged = false;
    private float mOffsetX = 0.0F;
    private double mSaturation = 0.0D;
    private OnPickerSaturationListener mListener;
    private ImageView rotationImageView;


    public ColorPickerView(Context context) {
        super(context);
    }

    public ColorPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setColorTemp(mColorTemp);
        if (!isChanged) {
            isChanged = true;
        }
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                pickerSaturationEvent(event);
                break;
            case MotionEvent.ACTION_DOWN:
                break;
            default:
                break;
        }

        return true;
    }

    public void setColorTemp(float paramFloat) {
        mColorTemp = paramFloat;
        if (rotationImageView != null) {
            paramFloat = paramFloat / 1000.0F * 280.0F - 180.0F - 50.0F;
            if (paramFloat < -180.0F) {
                paramFloat = paramFloat + 360.0F;
            }
            paramFloat = (float) (Math.PI - -((paramFloat / 180.0F) * Math.PI)) / 6.2831855F;
            rotationImageView.setRotation(paramFloat * 360.0F);
        }
    }

    private void initView() {
        rotationImageView = findViewById(R.id.rotationImageView);
    }

    private void pickerSaturationEvent(MotionEvent event) {
        float x = getPivotX();
        float y = getPivotY();
        double d2 = Math.atan2((event.getY() - y), (event.getX() - x)) *
                57.29577951308232 + 180 + 50;
        double d1 = d2;
        if (d2 > 360) {
            d1 = d2 - 360;
        }

        if (d1 <= 280) {
            mSaturation = d1 / 280 * 1000;
            mOffsetX = event.getX();
        } else if (mOffsetX - event.getX() < 0) {
            mSaturation = 0;
        } else {
            mSaturation = 1000;
        }

        if (event.getAction() != MotionEvent.ACTION_UP) {
            if (mListener != null) {
                mListener.onBeganPickerSaturation(mSaturation, 0);
            }
        } else if (mListener != null) {
            mListener.onEndedPickerSaturation(mSaturation, 0);
        }
        setColorTemp((float) mSaturation);
    }

    public void setOnPickerSaturationListener(OnPickerSaturationListener paramOnPickerSaturationListener) {
        mListener = paramOnPickerSaturationListener;
    }

    public interface OnPickerSaturationListener {
        void onBeganPickerSaturation(double param1Double1, double param1Double2);

        void onEndedPickerSaturation(double param1Double1, double param1Double2);
    }
}
