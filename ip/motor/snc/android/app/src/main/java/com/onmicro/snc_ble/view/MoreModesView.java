package com.onmicro.snc_ble.view;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.onmicro.snc_ble.R;

import java.util.ArrayList;

/**
 * @author peng
 */
public class MoreModesView extends ConstraintLayout {
    private ArrayList<ImageButton> dataSource = new ArrayList();

    private OnSelectMoreModesListener mListener;

    private PointF mPoint = new PointF(0.0F, 0.0F);

    private ImageButton mSelectedButton;

    private View.OnClickListener onMoreModesClick = param1View -> {
        int tag = (Integer) param1View.getTag();
        if (mListener != null) {
            mListener.onSelectMoreModesAtIndex(MoreModesView.this, tag);
        }

        if (tag != 5) {
            scrollToMoreModesAtIndex(tag);
        }
    };

    private int rows = 0;

    private View scrollIndicator;

    private int selectedIndex = 0;

    public MoreModesView(Context paramContext) {
        super(paramContext);
    }

    public MoreModesView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initView();
    }

    public MoreModesView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
    }

    private void initView() {
        if (getId() == R.id.moreModesView) {
            rows = 9;
            return;
        }
        rows = 12;
    }

    public int getResource(int paramInt, boolean paramBoolean) {
        String name;
        if (getId() != R.id.moreModesView) {
            if (paramBoolean) {
                name = loadResourcesName("moremodes1%02d_sel", paramInt);
            } else {
                name = loadResourcesName("moremodes1%02d_nor", paramInt);
            }
        } else {
            if (paramBoolean) {
                name = loadResourcesName("moremodes00%d_sel", paramInt);
            } else {
                name = loadResourcesName("moremodes00%d_nor", paramInt);
            }
        }
        return getResources().getIdentifier(name, "mipmap", getContext().getPackageName());
    }

    private String loadResourcesName(String name, int paramInt) {
        Object[] arrayOfObject = {paramInt};
        return String.format(name, arrayOfObject);
    }


    @Override
    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        scrollToMoreModesAtIndex(selectedIndex);
        if (mPoint.x != getWidth() && mPoint.y != getHeight()) {
            mPoint.x = getWidth();
            mPoint.y = getHeight();
            ConstraintLayout constraintLayout = findViewById(R.id.moreModesBackgroundView);
            constraintLayout.setMinWidth(getWidth() / 5 * this.rows);
            paramInt2 = this.dataSource.size();
            paramInt3 = 0;
            paramInt1 = paramInt3;
            if (paramInt2 == 0) {
                paramInt2 = 0;
                while (true) {
                    paramInt1 = paramInt3;
                    if (paramInt2 < this.rows) {
                        ImageButton imageButton = new ImageButton(getContext());
                        imageButton.setBackgroundColor(0);
                        imageButton.setImageResource(getResource(paramInt2, false));
                        imageButton.setScaleType(ImageView.ScaleType.CENTER);
                        imageButton.setTag(paramInt2);
                        imageButton.setOnClickListener(onMoreModesClick);
                        constraintLayout.addView(imageButton);
                        dataSource.add(imageButton);
                        paramInt2++;
                        continue;
                    }
                    break;
                }
            }
            while (paramInt1 < dataSource.size()) {
                ImageButton imageButton = dataSource.get(paramInt1);
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) imageButton.getLayoutParams();
                layoutParams.width = getWidth() / 5;
                layoutParams.height = getHeight();
                imageButton.setLayoutParams(layoutParams);
                imageButton.setX(getWidth() / 5.0F * paramInt1);
                imageButton.setY(0.0F);
                paramInt1++;
            }
            scrollIndicator = findViewById(R.id.scrollIndicator);
            scrollIndicator.getLayoutParams().width = (int) (getWidth() * 0.05F);
//            return;
        }
    }

    @Override
    protected void onMeasure(int paramInt1, int paramInt2) {
        super.onMeasure(paramInt1, paramInt2);
    }

    public void scrollToMoreModesAtIndex(int paramInt) {
        selectedIndex = Math.min(rows - 1, paramInt);
        if (dataSource.size() < this.rows)
            return;
        if (selectedIndex < 0) {
            if (mSelectedButton != null)
                mSelectedButton.setImageResource(getResource((Integer) mSelectedButton.getTag(), false));
            scrollIndicator.setVisibility(GONE);
            return;
        }
        scrollIndicator.setVisibility(VISIBLE);
        if (mSelectedButton != null)
            mSelectedButton.setImageResource(getResource((Integer) mSelectedButton.getTag(), false));
        mSelectedButton = dataSource.get(selectedIndex);
        mSelectedButton.setImageResource(getResource(selectedIndex, true));
        setScrollIndicatorOffset(mSelectedButton.getX());
    }

    public void setOnSelectMoreModesListener(OnSelectMoreModesListener paramOnSelectMoreModesListener) {
        this.mListener = paramOnSelectMoreModesListener;
    }

    public void setScrollIndicatorOffset(float paramFloat) {
        scrollIndicator.setX(paramFloat + ((mSelectedButton.getWidth() - scrollIndicator.getWidth()) / 2));
        scrollIndicator.setY((getHeight() - scrollIndicator.getHeight()));
    }

    public static interface OnSelectMoreModesListener {
        void onSelectMoreModesAtIndex(MoreModesView param1MoreModesView, int param1Int);
    }
}
