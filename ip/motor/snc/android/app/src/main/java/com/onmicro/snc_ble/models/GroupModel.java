package com.onmicro.snc_ble.models;

public class GroupModel {

    public static final int GROUPED_STYLE_WIVIN_GROOM = 0;

    public static final int GROUPED_STYLE_MASTER_BEDROOM = 1;

    public static final int GROUPED_STYLE_GUEST_ROOM = 2;

    public static final int GROUPED_STYLE_STUDY = 3;

    public static final int GROUPED_STYLE_KITCHEN = 4;

    public static final int GROUPED_STYLE_RESTAURANT = 5;

    public static final int GROUPED_STYLE_GUESTWC = 6;

    public static final int GROUPED_STYLE_BEDROOM = 7;

    public static final int GROUPED_STYLE_ADD_FAN = 8;

    public static final int GROUPED_STYLE_MASTER_CONTROL = 9;

    public static final int GROUPED_STYLE_ADD_GROUP = 10;


    private float brightness;

    private float colorTemp;

    private int featuresIndex;

    private String groupId;

    private String groupName;

    private Long id;

    private Boolean isOn;

    private Boolean isSelected;

    private float speed;

    private Boolean spread;

    private int style;

    private Long timeInterval;

    public GroupModel() {
    }

    public GroupModel(Long paramLong1, int paramInt1, String paramString1, String paramString2, float paramFloat1, float paramFloat2, Long paramLong2, int paramInt2, float paramFloat3, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3) {
        this.id = paramLong1;
        this.style = paramInt1;
        this.groupId = paramString1;
        this.groupName = paramString2;
        this.colorTemp = paramFloat1;
        this.brightness = paramFloat2;
        this.timeInterval = paramLong2;
        this.featuresIndex = paramInt2;
        this.speed = paramFloat3;
        this.isSelected = paramBoolean1;
        this.isOn = paramBoolean2;
        this.spread = paramBoolean3;
    }

    public float getBrightness() {
        return this.brightness;
    }

    public float getColorTemp() {
        return this.colorTemp;
    }

    public int getFeaturesIndex() {
        return this.featuresIndex;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getIsOn() {
        return this.isOn;
    }

    public Boolean getIsSelected() {
        return this.isSelected;
    }

    public float getSpeed() {
        return this.speed;
    }

    public Boolean getSpread() {
        return this.spread;
    }

    public int getStyle() {
        return style;
    }

    public Long getTimeInterval() {
        return this.timeInterval;
    }

    public void setBrightness(float paramFloat) {
        this.brightness = paramFloat;
    }

    public void setColorTemp(float paramFloat) {
        this.colorTemp = paramFloat;
    }

    public void setFeaturesIndex(int paramInt) {
        this.featuresIndex = paramInt;
    }

    public void setGroupId(String paramString) {
        this.groupId = paramString;
    }

    public void setGroupName(String paramString) {
        this.groupName = paramString;
    }

    public void setId(Long paramLong) {
        this.id = paramLong;
    }

    public void setIsOn(Boolean paramBoolean) {
        this.isOn = paramBoolean;
    }

    public void setIsSelected(Boolean paramBoolean) {
        this.isSelected = paramBoolean;
    }

    public void setSpeed(float paramFloat) {
        this.speed = paramFloat;
    }

    public void setSpread(Boolean paramBoolean) {
        this.spread = paramBoolean;
    }

    public void setStyle(int paramInt) {
        this.style = paramInt;
    }

    public void setTimeInterval(Long paramLong) {
        this.timeInterval = paramLong;
    }
}
