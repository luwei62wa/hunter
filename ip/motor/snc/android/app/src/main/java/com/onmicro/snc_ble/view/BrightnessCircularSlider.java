package com.onmicro.snc_ble.view;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.onmicro.snc_ble.R;

/**
 * @author PengYaQuan
 * @create 2019-10-22 15:36
 * @Describe
 */
public class BrightnessCircularSlider extends ConstraintLayout {

    private float brightnessValue = 0.0F;

    private boolean isChanged = false;

    private OnUpdateBrightnessListener mListener;

    private ImageView thumbImageView;

    public BrightnessCircularSlider(Context context) {
        super(context);
    }

    public BrightnessCircularSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public BrightnessCircularSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (!isChanged) {
            isChanged = true;
            int width = getWidth();
            ConstraintLayout.LayoutParams params = (LayoutParams) thumbImageView.getLayoutParams();
            left = (int) (width * 0.07);
            params.width = left;
            params.height = left;
            thumbImageView.setLayoutParams(params);
            reloadBrightnessSlider(brightnessValue);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_DOWN:

                brightnessSliderEvent(event);
                break;
            default:
                break;
        }
        return true;
    }

    private void initView() {
        thumbImageView = new ImageView(getContext());
        thumbImageView.setImageResource(R.mipmap.brightness_slider_thumb);
        addView(thumbImageView);
    }

    public void reloadBrightnessSlider(float paramFloat) {
        brightnessValue = paramFloat;
        if (thumbImageView != null) {
            paramFloat = -(((1F - paramFloat / 1000F) * 0.18F + 0.41F) * 3.1415927F);
            float f1 = getWidth() * 1.5F;
            PointF pointf = new PointF(0, 0);
            double d1 = Math.abs(f1);
            pointf.x = getPivotX() + (float) (d1 * Math.cos(paramFloat));
            pointf.y = 1.051F * f1 + (float) (d1 * Math.sin(paramFloat));
            thumbImageView.setX(pointf.x - thumbImageView.getPivotX());
            thumbImageView.setY(pointf.y - thumbImageView.getPivotY());
        }
    }


    private void brightnessSliderEvent(MotionEvent event) {
        float min = Math.min(1000.0F, Math.max(0.0F, (event.getX() - getWidth() * 0.1F) / getWidth() * 1.2F * 1000.0F));

//        Log.e("test", "min:" + min + "  event.getx:" + event.getX() + "   width:" + getWidth());
        reloadBrightnessSlider(min);

        if (event.getAction() != MotionEvent.ACTION_UP) {
            if (mListener != null) {
                mListener.onBeganUpdateBrightness(min);
            }
        } else if (mListener != null) {
            mListener.onEndedUpdateBrightness(min);
        }
    }


    public void setOnUpdateBrightnessListener(OnUpdateBrightnessListener paramOnUpdateBrightnessListener) {
        this.mListener = paramOnUpdateBrightnessListener;
    }

    public static interface OnUpdateBrightnessListener {
        void onBeganUpdateBrightness(float param1Float);

        void onEndedUpdateBrightness(float param1Float);
    }
}
