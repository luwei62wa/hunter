package com.onmicro.snc_ble.app;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.onmicro.snc_ble.core.db.DaoManager;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;

import java.util.Iterator;

/**
 * @author peng
 */
public class App extends Application {
    private static DisplayMetrics _metrics;

//    private static App instance;

    private static Context mAppContext;

    private static Context mContext;

    public App() {
    }

//    public static App getAppInstance() {
//        if (instance == null) {
//            synchronized (App.class) {
//                if (instance == null) {
//                    return instance = new App();
//                }
//            }
//        }
//        return instance;
//    }

    private String getAppName(int paramInt) {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        PackageManager packageManager = getPackageManager();
        Iterator<ActivityManager.RunningAppProcessInfo> iterator;
        if (activityManager != null) {
            iterator = activityManager.getRunningAppProcesses().iterator();
            try {
                while (iterator.hasNext()) {
                    ActivityManager.RunningAppProcessInfo runningAppProcessInfo = iterator.next();
                    if (runningAppProcessInfo.pid == paramInt) {
                        packageManager.getApplicationLabel(packageManager.getApplicationInfo(runningAppProcessInfo.processName, PackageManager.GET_META_DATA));
                        return runningAppProcessInfo.processName;
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Context getContext() {
        return mContext;
    }

    public static Context getInstance() {
        return mAppContext;
    }

    public static DisplayMetrics getMetrics() {
        return _metrics;
    }

    public static void setContext(Context paramContext) {
        mContext = paramContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();
        DaoManager.getInstance().init(mAppContext);
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        _metrics = new DisplayMetrics();
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(_metrics);
        }
        String stringBuilder = SpeechConstant.APPID +"=5b14e598";
        SpeechUtility.createUtility(this, stringBuilder);
    }


    //    public void onCreate() {
//        super.onCreate();
//        mAppContext = getApplicationContext();
//        DaoManager.getInstance().init(mAppContext);
//        WindowManager windowManager = (WindowManager) getSystemService("window");
//        _metrics = new DisplayMetrics();
//        windowManager.getDefaultDisplay().getMetrics(_metrics);
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("appid=");
//        stringBuilder.append(getString(2131492904));
//        SpeechUtility.createUtility(this, stringBuilder.toString());
//    }

}

