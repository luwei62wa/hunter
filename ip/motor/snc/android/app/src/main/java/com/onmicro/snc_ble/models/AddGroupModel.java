package com.onmicro.snc_ble.models;

public class AddGroupModel {
    private String groupName;

    private Boolean isSelected;

    private int style;

    public String getGroupName() {
        return this.groupName;
    }

    public Boolean getSelected() {
        return this.isSelected;
    }

    public int getStyle() {
        return this.style;
    }

    public void setGroupName(String paramString) {
        this.groupName = paramString;
    }

    public void setSelected(Boolean paramBoolean) {
        this.isSelected = paramBoolean;
    }

    public void setStyle(int paramInt) {
        this.style = paramInt;
    }
}
