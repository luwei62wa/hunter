package com.onmicro.snc_ble.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.onmicro.snc_ble.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class OptAnimationLoader {

    private static Animation createAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
        return createAnimationFromXml(paramContext, paramXmlPullParser, null, Xml.asAttributeSet(paramXmlPullParser));
    }

    private static Animation createAnimationFromXml(Context c, XmlPullParser parser, AnimationSet parent, AttributeSet attrs) throws XmlPullParserException, IOException {
        Animation anim = null;

        // Make sure we are on a start tag.
        int type;
        int depth = parser.getDepth();

        while (((type=parser.next()) != XmlPullParser.END_TAG || parser.getDepth() > depth)
                && type != XmlPullParser.END_DOCUMENT) {

            if (type != XmlPullParser.START_TAG) {
                continue;
            }

            String  name = parser.getName();

            if (name.equals("set")) {
                anim = new AnimationSet(c, attrs);
                createAnimationFromXml(c, parser, (AnimationSet)anim, attrs);
            } else if (name.equals("alpha")) {
                anim = new AlphaAnimation(c, attrs);
            } else if (name.equals("scale")) {
                anim = new ScaleAnimation(c, attrs);
            }  else if (name.equals("rotate")) {
                anim = new RotateAnimation(c, attrs);
            }  else if (name.equals("translate")) {
                anim = new TranslateAnimation(c, attrs);
            } else {
                throw new RuntimeException("Unknown animation name: " + parser.getName());
            }

            if (parent != null) {
                parent.addAnimation(anim);
            }
        }

        return anim;
    }

    public static Animation loadAnimation(Context paramContext, int paramInt) throws
            Resources.NotFoundException {
        XmlResourceParser animation;
        try {
            animation = paramContext.getResources().getAnimation(R.anim.modal_in);
//            paramContext.getResources();
            return createAnimationFromXml(paramContext, animation);
        } catch (Exception e) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Can't load animation resource ID #0x");
            stringBuilder1.append(Integer.toHexString(paramInt));
            Resources.NotFoundException notFoundException1 = new Resources.NotFoundException(stringBuilder1.toString());
            notFoundException1.initCause(e);
            throw notFoundException1;
        }
    }
}
